// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <deque>
#include <mutex>
#include <utility>

namespace qs
{
	template <typename T>
	struct TsQueue
	{
		bool empty() const
		{
			std::lock_guard<std::mutex> _(m_);
			return q_.empty();
		}

		void push(T const& value)
		{
			std::lock_guard<std::mutex> _(m_);
			q_.push_back(value);
		}

		void push(T&& value)
		{
			std::lock_guard<std::mutex> _(m_);
			q_.push_back(std::move(value));
		}

		T pop()
		{
			std::unique_lock<std::mutex> lock(m_);
			auto value = std::move(q_.front());
			q_.pop_front();
			return value;
		}

		bool tryPop(T& out)
		{
			std::unique_lock<std::mutex> lock(m_);

			if(q_.empty())
				return false;

			out = std::move(q_.front());
			q_.pop_front();
			return true;
		}

		std::deque<T> takeAll()
		{
			std::unique_lock<std::mutex> lock(m_);
			auto q = std::move(q_);
			q_ = std::deque<T>();
			return q;
		}

	private:
		std::deque<T> q_;
		mutable std::mutex m_;
	};
}
