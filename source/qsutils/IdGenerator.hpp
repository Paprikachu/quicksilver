// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <limits>
#include <stdexcept>
#include <vector>

namespace qs
{
	template <typename Id>
	struct IdGenerator
	{
		Id createId()
		{
			if(!freeList_.empty())
			{
				auto id = freeList_.back();
				freeList_.pop_back();
				return id;
			}

			if(nextId_ == std::numeric_limits<Id>::max())
				throw std::runtime_error("out of ids");

			return nextId_++;
		}

		void destroyId(Id id)
		{
			freeList_.push_back(id);
		}

	private:
		std::vector<Id> freeList_;
		Id nextId_ = static_cast<Id>(0);
	};
}
