// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <utility>

#include "ArrayMap.hpp"
#include "Assertions.hpp"
#include "IdGenerator.hpp"
#include "Types.hpp"

namespace qs
{
	template <typename T>
	struct ArraySet
	{
		template <typename... Args>
		SizeT emplace(Args&&... args)
		{
			auto index = idgen_.createId();
			QS_ASSERT(values_.emplace(index, std::forward<Args>(args)...).first, "value could not be inserted into ArraySet at index '%s'", index)
			return index;
		}

		void erase(SizeT index)
		{
			QS_ASSERT(values_.erase(index), "value could not be erased from ArraySet at index '%s'", index)
			idgen_.destroyId(index);
		}

		template <typename F>
		void iterate(F&& f)
		{
			for(auto kv : values_)
				f(kv.value);
		}

		T& operator [] (SizeT index) { return *values_.find(index); }
		T const& operator [] (SizeT index) const { return *values_.find(index); }

	private:
		IdGenerator<SizeT> idgen_;
		ArrayMap<SizeT, T> values_;
	};
}
