// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <type_traits>
#include <vector>

#include <boost/algorithm/string/split.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/system/error_code.hpp>

#include <json/json.h>

#include "FileUtils.hpp"
#include "Format.hpp"
#include "Identity.hpp"
#include "NamespaceShorthands.hpp"
#include "Types.hpp"

namespace qs
{
	namespace detail
	{
		inline bool is(Json::Value const& value, Identity<bool>)
		{
			return value.isBool();
		}

		inline bool is(Json::Value const& value, Identity<std::string>)
		{
			return value.isString();
		}

		inline bool is(Json::Value const& value, Identity<Float64>)
		{
			return value.isDouble();
		}

		template <typename Int, typename std::enable_if<std::is_integral<Int>::value>::type* = nullptr>
		bool is(Json::Value const& value, Identity<Int>)
		{
			return std::is_unsigned<Int>::value ? value.isUInt64() : value.isInt64();
		}

		inline bool as(Json::Value const& value, Identity<bool>)
		{
			return value.asBool();
		}

		inline std::string as(Json::Value const& value, Identity<std::string>)
		{
			return value.asString();
		}

		inline Float64 as(Json::Value const& value, Identity<Float64>)
		{
			return value.asDouble();
		}

		inline UInt64 valueToInt(Json::Value const& value, std::true_type)
		{
			return value.asUInt64();
		}

		inline Int64 valueToInt(Json::Value const& value, std::false_type)
		{
			return value.asInt64();
		}

		template <typename Int, typename std::enable_if<std::is_integral<Int>::value>::type* = nullptr>
		inline Int as(Json::Value const& value, Identity<Int>)
		{
			auto result = valueToInt(value, std::integral_constant<bool, std::is_unsigned<Int>::value>());
			auto min = std::numeric_limits<Int>::min();
			auto max = std::numeric_limits<Int>::max();

			if(result < min || result > max)
				throw std::runtime_error(format("value is not in the range [%s, %s] (got %s)", min, max, result));

			return static_cast<Int>(result);
		}

		inline Int64 as(Json::Value const& value, Identity<Int64>)
		{
			return value.asInt64();
		}

		inline UInt64 as(Json::Value const& value, Identity<UInt64>)
		{
			return value.asUInt64();
		}
	}

	template <typename T>
	bool is(Json::Value const& value)
	{
		return detail::is(value, Identity<T>());
	}

	template <typename T>
	T as(Json::Value const& value)
	{
		return detail::as(value, Identity<T>());
	}

	inline Json::Value const& resolvePath(Json::Value const& root, std::string const& path)
	{
		std::vector<std::string> elements;
		boost::algorithm::split(elements, path, [](char c){ return c == '.'; });

		Json::Value const* result = &root;

		for(auto& elem : elements)
			result = &(*result)[elem];

		if(result->isNull())
			throw std::runtime_error(format("%s not found", path));

		return *result;
	}

	template <typename T>
	T find(Json::Value const& root, std::string const& path)
	{
		auto result = resolvePath(root, path);

		if(!is<T>(result))
			throw std::runtime_error(format("%s is not of correct type", path));

		return as<T>(result);
	}

	template <typename T>
	T findInRange(Json::Value const& root, std::string const& path, T min, T max)
	{
		auto result = find<T>(root, path);

		if(result < min || result > max)
			throw std::runtime_error(format("%s is not in the range [%s, %s] (got %s)", path, min, max, result));

		return result;
	}

	inline bai::address findIp(Json::Value const& root, std::string const& path)
	{
		auto str = find<std::string>(root, path);

		bs::error_code ec;
		auto result = bai::address::from_string(str, ec);

		if(ec)
			throw std::runtime_error(format("%s is not a valid ip address (got \"%s\")", path, str));

		return result;
	}

	inline Json::Value loadJsonFromFile(std::string const& name)
	{
		std::vector<UInt8> data;

		if(!readFile(name, data))
			throw std::runtime_error(format("failed to open file '%s'", name));

		Json::Reader reader;
		Json::Value config;

		auto begin = reinterpret_cast<char const*>(data.data());

		if(!reader.parse(begin, begin + data.size(), config))
			throw std::runtime_error(format("file '%s' is not valid json", name));

		return config;
	}
}
