// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <utility>

#include "DllExport.hpp"
#include "Format.hpp"

namespace qs
{
	namespace detail
	{
		QS_DLL_EXPORT
		void* libraryOpen(std::string const& file);

		QS_DLL_EXPORT
		void libraryClose(void* library);

		QS_DLL_EXPORT
		void* libraryFindSymbol(void* library, char const* symbol);

		QS_DLL_EXPORT
		std::string libraryLastError();
	}

	struct Library
	{
		Library() : handle_(nullptr) {}
		Library(void* handle) : handle_(handle) {}

		Library(Library&& other) : handle_(std::exchange(other.handle_, nullptr)) {}
		Library& operator = (Library&& other) { handle_ = std::exchange(other.handle_, nullptr); return *this; }

		Library(Library const&) = delete;
		Library& operator = (Library const&) = delete;

		~Library()
		{
			if (handle_)
				detail::libraryClose(handle_);
		}

		static Library open(std::string const& file)
		{
			auto handle = detail::libraryOpen(file);

			if(!handle)
			{
				// this has to be guaranteed first, because the last error is only valid until here (on windows)
				auto errmsg = detail::libraryLastError();
				throw std::runtime_error(format("failed to open %s: %s", file, errmsg));
			}

			return Library(handle);
		}

		void* find(std::string const& symbol)
		{
			return detail::libraryFindSymbol(handle_, symbol.c_str());
		}

		explicit operator bool() const
		{
			return handle_;
		}

	private:
		void* handle_;
	};
}
