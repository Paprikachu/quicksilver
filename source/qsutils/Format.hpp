// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>

#include <boost/format.hpp>

#include "Fold.hpp"
#include "LambdaOperators.hpp"

namespace qs
{
	template <typename... Args>
	std::string format(std::string const& fmt, Args const&... args)
	{
		return foldl(boost::format(fmt), QS_INFIX_OP_FN(%), args...).str();
	}
}
