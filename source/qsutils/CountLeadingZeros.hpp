// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <type_traits>

#include <boost/predef.h>

#include "Types.hpp"

#if BOOST_COMP_MSVC
#include <intrin.h>
#endif

namespace qs
{
#if BOOST_COMP_MSVC

	// http://stackoverflow.com/questions/355967/how-to-use-msvc-intrinsics-to-get-the-equivalent-of-this-gcc-code

	inline SizeT countLeadingZerosImpl(unsigned x)
	{
		unsigned long leadingZeros;
		return _BitScanReverse(&leadingZeros, x) ? 31 - leadingZeros : 32;
	}

	inline SizeT countLeadingZerosImpl(unsigned long x)
	{
		unsigned long leadingZeros;
		return _BitScanReverse(&leadingZeros, x) ? 31 - leadingZeros : 32;
	}

	inline SizeT countLeadingZerosImpl(unsigned long long x)
	{
		unsigned long leadingZeros;
		return _BitScanReverse64(&leadingZeros, x) ? 63 - leadingZeros : 64;
	}

	inline SizeT countLeadingZerosImpl(unsigned char x)
	{
		return countLeadingZerosImpl(static_cast<unsigned>(x)) - 24;
	}

	inline SizeT countLeadingZerosImpl(unsigned short x)
	{
		return countLeadingZerosImpl(static_cast<unsigned>(x)) - 16;
	}

#else

	inline SizeT countLeadingZerosImpl(unsigned x)
	{
		return __builtin_clz(x);
	}

	inline SizeT countLeadingZerosImpl(unsigned long x)
	{
		return __builtin_clzl(x);
	}

	inline SizeT countLeadingZerosImpl(unsigned long long x)
	{
		return __builtin_clzll(x);
	}

	inline SizeT countLeadingZerosImpl(unsigned char x)
	{
		return countLeadingZerosImpl(static_cast<unsigned>(x)) - 24;
	}

	inline SizeT countLeadingZerosImpl(unsigned short x)
	{
		return countLeadingZerosImpl(static_cast<unsigned>(x)) - 16;
	}

#endif

	template <typename T>
	SizeT countLeadingZeros(T value)
	{
		using UT = typename std::make_unsigned<T>::type;
		return countLeadingZerosImpl(static_cast<UT>(value));
	}
}
