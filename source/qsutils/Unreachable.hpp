// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "DllExport.hpp"
#include "NoReturn.hpp"
#include "Types.hpp"

#define QS_UNREACHABLE(msg) qs::unreachable(__FILE__, __LINE__, __func__, msg);

namespace qs
{
	QS_NORETURN
	QS_DLL_EXPORT
	void unreachable(char const* file, SizeT line, char const* function, char const* message);
}
