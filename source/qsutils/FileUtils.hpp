// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <vector>

#include "DllExport.hpp"
#include "Types.hpp"

namespace qs
{
	QS_DLL_EXPORT
	bool readFile(std::string const& path, std::vector<UInt8>& bytes);

	namespace OpenMode
	{
		enum OpenMode
		{
			ACCESS_READ  = 1,
			ACCESS_WRITE = 2,
		};
	}

	enum struct SeekType : unsigned
	{
		BEGIN,
		CURRENT,
		END,
	};

	struct File
	{
		File();
		File(File&& other);
		File& operator = (File other);
		~File();

		File(void* handle);

		void* handle();

		static File open(std::string const& path, OpenMode::OpenMode mode);

		SizeT size();

		SizeT read(void* buf, SizeT size);
		void write(void const* buf, SizeT size);

		void truncate();

		void flush();

		void seek(SizeT offset, SeekType type);
		SizeT tell();

	private:
		void* handle_;
	};
}
