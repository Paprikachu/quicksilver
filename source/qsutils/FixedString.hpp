// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <algorithm>
#include <string>
#include <ostream>

#include "Assertions.hpp"

namespace qs
{
	template <typename Char, typename Length, Length Capacity>
	struct FixedString
	{
		using Iterator = Char*;
		using ConstIterator = Char const*;

		FixedString() : length_() {}
		FixedString(Char const* ptr, Length length) : length_() { append(ptr, length); }
		FixedString(Length length) : data_{}, length_(length) {}

		Char& operator [] (Length index) { return data_[index]; }
		Char operator [] (Length index) const { return data_[index]; }

		Char* data() { return data_; }
		Char const* data() const { return data_; }

		bool empty() const { return length_ == 0; }
		Length length() const { return length_; }

		Iterator begin() { return data_; }
		Iterator end() { return data_ + length_; }

		ConstIterator begin() const { return data_; }
		ConstIterator end() const { return data_ + length_; }

		FixedString& operator += (Char c)
		{
			append(&c, 1);
			return *this;
		}

		FixedString& operator += (FixedString const& rhs)
		{
			append(rhs.data_, rhs.length_);
			return *this;
		}

		void append(Char const* ptr, Length count)
		{
			QS_ASSERT(Capacity - length_ >= count, "out of space")

			// memcpy requires an include
			while(count--)
				data_[length_++] = *ptr++;
		}

		template <typename Traits = std::char_traits<Char>, typename Alloc = std::allocator<Char>>
		std::basic_string<Char, Traits, Alloc> toStdString() const
		{
			return std::basic_string<Char, Traits, Alloc>(data_, data_ + length_);
		}

		template <SizeT N>
		static FixedString fromCString(Char const (&s) [N])
		{
			static_assert(N - 1 <= Capacity, "string literal too long");
			return FixedString(s, N - 1);
		}

		template <typename Traits, typename Alloc>
		static FixedString fromStdString(std::basic_string<Char, Traits, Alloc> const& s)
		{
			QS_ASSERT(s.length() <= Capacity, "string too long")
			return FixedString(s.data(), static_cast<Length>(s.length()));
		}

	private:
		Char data_[Capacity];
		Length length_;
	};

	template <typename Char, typename Length, Length Capacity>
	FixedString<Char, Length, Capacity> operator + (FixedString<Char, Length, Capacity> lhs, FixedString<Char, Length, Capacity> const& rhs)
	{
		return lhs += rhs;
	}

	template <typename Char, typename Length, Length Capacity>
	bool operator == (FixedString<Char, Length, Capacity> const& lhs, FixedString<Char, Length, Capacity> const& rhs)
	{
		return lhs.length() == rhs.length() && std::equal(lhs.begin(), lhs.end(), rhs.begin());
	}

	template <typename Char, typename Length, Length Capacity>
	bool operator != (FixedString<Char, Length, Capacity> const& lhs, FixedString<Char, Length, Capacity> const& rhs)
	{
		return !(lhs == rhs);
	}

	template <typename Char, typename Length, Length Capacity>
	bool operator < (FixedString<Char, Length, Capacity> const& lhs, FixedString<Char, Length, Capacity> const& rhs)
	{
		return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

	template <typename Char, typename Length, Length Capacity>
	bool operator >= (FixedString<Char, Length, Capacity> const& lhs, FixedString<Char, Length, Capacity> const& rhs)
	{
		return !(lhs < rhs);
	}

	template <typename Char, typename Length, Length Capacity>
	bool operator > (FixedString<Char, Length, Capacity> const& lhs, FixedString<Char, Length, Capacity> const& rhs)
	{
		return rhs < lhs;
	}

	template <typename Char, typename Length, Length Capacity>
	bool operator <= (FixedString<Char, Length,Capacity> const& lhs, FixedString<Char, Length, Capacity> const& rhs)
	{
		return !(lhs > rhs);
	}

	template <typename Char, typename Length, Length Capacity, typename Traits>
	std::basic_ostream<Char, Traits>& operator << (std::basic_ostream<Char, Traits>& os, FixedString<Char, Length, Capacity> const& s)
	{
		return os.write(s.data(), s.length());
	}
}
