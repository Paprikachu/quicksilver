// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Types.hpp"

namespace qs
{
	template <typename Hasher>
	struct UHash
	{
		template <typename T>
		SizeT operator () (T const& value) const
		{
			Hasher hasher;
			hashAppend(hasher, value);
			return hasher.finalize();
		}
	};

	template <typename Hasher>
	void hashAppend(Hasher& hasher, void const* data, SizeT size)
	{
		hasher.feed(data, size);
	}
}
