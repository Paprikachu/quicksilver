// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <functional>
#include <type_traits>

namespace qs
{
	template <typename T>
	struct PtrCompare
	{
		using is_transparent = std::true_type;

		template <typename P1, typename P2>
		bool operator () (P1& p1, P2& p2) const
		{
			return less(p1.get(), p2.get());
		}

		template <typename P1>
		bool operator () (P1& p1, T* p2) const
		{
			return less(p1.get(), p2);
		}

		template <typename P2>
		bool operator () (T* p1, P2& p2) const
		{
			return less(p1, p2.get());
		}

		bool operator () (T* p1, T* p2) const
		{
			return less(p1, p2);
		}

	private:
		static bool less(T* p1, T* p2)
		{
			return std::less<T*>()(p1, p2);
		}
	};
}
