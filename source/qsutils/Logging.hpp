// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>

#include "DllExport.hpp"
#include "Format.hpp"

namespace qs
{
	namespace detail
	{
		QS_DLL_EXPORT
		std::string currentTimeString();

		QS_DLL_EXPORT
		void doLog(std::string const& str);

		template <typename... Args>
		void log(std::string const& fmt, char const* type, Args const&... args)
		{
			doLog(format("[%s] %s %s", currentTimeString(), type, format(fmt, args...)));
		}
	}

	template <typename... Args>
	void info(std::string const& fmt, Args const&... args)
	{
		detail::log(fmt, "<INFO>   ", args...);
	}

	template <typename... Args>
	void warning(std::string const& fmt, Args const&... args)
	{
		detail::log(fmt, "<WARNING>", args...);
	}

	template <typename... Args>
	void error(std::string const& fmt, Args const&... args)
	{
		detail::log(fmt, "<ERROR>  ", args...);
	}
}
