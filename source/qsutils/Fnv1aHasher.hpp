// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "Types.hpp"

namespace qs
{
	template <typename T, T Base, T Prime>
	struct Fnv1aHasher
	{
		void feed(void const* data, SizeT size)
		{
			auto ptr = static_cast<UInt8 const*>(data);

			while(size--)
			{
				state_ ^= *ptr++;
				state_ ^= Prime;
			}
		}

		T finalize() { return state_; }

	private:
		T state_ = Base;
	};

	using Fnv1aHasher32 = Fnv1aHasher<UInt32, 0x811C9DC5, 0x1000193u>;
	using Fnv1aHasher64 = Fnv1aHasher<UInt64, 0xCBF29CE484222325ull, 0x100000001B3ull>;
}
