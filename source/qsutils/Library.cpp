// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <boost/predef.h>

#include "Library.hpp"

#if BOOST_OS_WINDOWS
#define QS_LIB_EXTENSION ".dll"
#elif BOOST_OS_MACOS
#define QS_LIB_EXTENSION ".dylib"
#else
#define QS_LIB_EXTENSION ".so"
#endif

#if BOOST_OS_WINDOWS

#include "Windows.hpp"

namespace qs
{
	namespace detail
	{
		QS_DLL_EXPORT
		void* libraryOpen(std::string const& file)
		{
			return ::LoadLibraryW(widen(file + QS_LIB_EXTENSION).c_str());
		}

		QS_DLL_EXPORT
		void libraryClose(void* library)
		{
			::FreeLibrary(static_cast<HMODULE>(library));
		}

		QS_DLL_EXPORT
		void* libraryFindSymbol(void* library, char const* symbol)
		{
			return reinterpret_cast<void*>(::GetProcAddress(static_cast<HMODULE>(library), symbol));
		}

		QS_DLL_EXPORT
		std::string libraryLastError()
		{
			return getLastError();
		}
	}
}

#else

#include <dlfcn.h>

namespace qs
{
	namespace detail
	{
		QS_DLL_EXPORT
		void* libraryOpen(std::string const& file)
		{
			return dlopen((file + QS_LIB_EXTENSION).c_str(), RTLD_NOW);
		}

		QS_DLL_EXPORT
		void libraryClose(void* library)
		{
			dlclose(library);
		}

		QS_DLL_EXPORT
		void* libraryFindSymbol(void* library, char const* symbol)
		{
			return dlsym(library, symbol);
		}

		QS_DLL_EXPORT
		std::string libraryLastError()
		{
			return dlerror();
		}
	}
}

#endif
