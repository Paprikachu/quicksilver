// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <locale>
#include <memory>
#include <mutex>
#include <sstream>

#include <boost/date_time.hpp>

#include "Logging.hpp"

namespace qs
{
	namespace
	{
		std::unique_ptr<std::ostringstream> makeTimeFormatter(char const* fmt)
		{
			auto oss = std::make_unique<std::ostringstream>();
			auto facet = new boost::posix_time::time_facet(fmt);
			oss->imbue(std::locale(oss->getloc(), facet));
			return oss;
		}

		std::mutex mutex;
		auto formatter = makeTimeFormatter("%Y-%m-%d %H:%M:%S.%f");
	}

	namespace detail
	{
		QS_DLL_EXPORT
		std::string currentTimeString()
		{
			formatter->str("");
			*formatter << boost::posix_time::microsec_clock::local_time();
			return formatter->str();
		}

		QS_DLL_EXPORT
		void doLog(std::string const& s)
		{
			std::lock_guard<std::mutex> _(mutex);
			std::cout << s << '\n';
			std::cout.flush();
		}
	}
}
