// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdexcept>
#include <utility>

#include <boost/numeric/conversion/cast.hpp>
#include <boost/predef.h>

#include "Assertions.hpp"
#include "FileUtils.hpp"

#if BOOST_OS_WINDOWS
#include "Windows.hpp"
#else
#include <cerrno>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

namespace qs
{
#if BOOST_OS_WINDOWS

	HANDLE toNativeHandle(void* handle)
	{
		return handle;
	}

	void* fromNativeHandle(HANDLE handle)
	{
		return handle;
	}

#define QS_FILE_INVALID_HANDLE ((void*)INVALID_HANDLE_VALUE)

#else

	int toNativeHandle(void* handle)
	{
		return static_cast<int>(reinterpret_cast<std::uintptr_t>(handle));
	}

	void* fromNativeHandle(int handle)
	{
		return reinterpret_cast<void*>(static_cast<std::uintptr_t>(handle));
	}

#define QS_FILE_INVALID_HANDLE ((void*)((std::uintptr_t)-1))

#endif

	QS_DLL_EXPORT
	bool readFile(std::string const& path, std::vector<UInt8>& bytes)
	{
		try
		{
			auto file = File::open(path, OpenMode::ACCESS_READ);
			auto size = file.size();

			bytes.resize(size);

			QS_ASSERT(file.read(bytes.data(), bytes.size()) == size, "eof reached unexpectedly")

			return true;
		}

		catch(std::runtime_error const&)
		{}

		return false;
	}

	File::File()
		: handle_(QS_FILE_INVALID_HANDLE)
	{}

	File::File(File&& other)
		: handle_(std::exchange(other.handle_, QS_FILE_INVALID_HANDLE))
	{}

	File& File::operator = (File other)
	{
		handle_ = std::exchange(other.handle_, QS_FILE_INVALID_HANDLE);
		return *this;
	}

	File::File(void* handle)
		: handle_(handle)
	{}

	void* File::handle()
	{
		return handle_;
	}

#if BOOST_OS_WINDOWS

	File::~File()
	{
		if(handle_)
			CloseHandle(toNativeHandle(handle_));
	}

	File File::open(std::string const& path, OpenMode::OpenMode mode)
	{
		DWORD access = 0;

		if(mode & OpenMode::ACCESS_READ)
			access |= GENERIC_READ;

		if(mode & OpenMode::ACCESS_WRITE)
			access |= GENERIC_WRITE;

		DWORD creation = mode & OpenMode::ACCESS_WRITE ? OPEN_ALWAYS : OPEN_EXISTING;

		auto handle = fromNativeHandle(CreateFileW(widen(path).c_str(), access, 0, nullptr, creation, FILE_ATTRIBUTE_NORMAL, nullptr));

		if(handle == QS_FILE_INVALID_HANDLE)
			throw std::runtime_error(getLastError());

		return File(handle);
	}

	SizeT File::size()
	{
		LARGE_INTEGER size;

		if(!GetFileSizeEx(toNativeHandle(handle_), &size))
			throw std::runtime_error(getLastError());

		return boost::numeric_cast<SizeT>(size.QuadPart);
	}

	SizeT File::read(void* buf, SizeT size)
	{
		DWORD bytesRead;

		if(!ReadFile(toNativeHandle(handle_), buf, boost::numeric_cast<DWORD>(size), &bytesRead, nullptr))
			throw std::runtime_error(getLastError());

		return boost::numeric_cast<SizeT>(bytesRead);
	}

	void File::write(void const* buf, SizeT size)
	{
		DWORD bytesWritten;

		if(!WriteFile(toNativeHandle(handle_), buf, boost::numeric_cast<DWORD>(size), &bytesWritten, nullptr))
			throw std::runtime_error(getLastError());

		QS_ASSERT(bytesWritten == size, "WriteFile() returned with less bytes written than requested")
	}

	void File::truncate()
	{
		if(!SetEndOfFile(toNativeHandle(handle_)))
			throw std::runtime_error(getLastError());
	}

	void File::flush()
	{
		if(!FlushFileBuffers(toNativeHandle(handle_)))
			throw std::runtime_error(getLastError());
	}

	void File::seek(SizeT offset, SeekType type)
	{
		static constexpr DWORD seekTypes[] = { FILE_BEGIN, FILE_CURRENT, FILE_END };

		LARGE_INTEGER largeOffset;
		largeOffset.QuadPart = boost::numeric_cast<decltype(largeOffset.QuadPart)>(offset);

		if(!SetFilePointerEx(toNativeHandle(handle_), largeOffset, nullptr, seekTypes[static_cast<unsigned>(type)]))
			throw std::runtime_error(getLastError());
	}

	SizeT File::tell()
	{
		LARGE_INTEGER offset;
		offset.QuadPart = 0;

		if(!SetFilePointerEx(toNativeHandle(handle_), offset, &offset, FILE_CURRENT))
			throw std::runtime_error(getLastError());

		return boost::numeric_cast<SizeT>(offset.QuadPart);
	}

#else

	File::~File()
	{
		if(handle_ != QS_FILE_INVALID_HANDLE)
			close(toNativeHandle(handle_));
	}

	File File::open(std::string const& path, OpenMode::OpenMode mode)
	{
		static constexpr int openModes[] = { 0, O_RDONLY, O_WRONLY, O_RDWR };

		auto flags = openModes[mode] | (mode & OpenMode::ACCESS_WRITE ? O_CREAT : 0) | O_CLOEXEC;
		auto handle = fromNativeHandle(::open(path.c_str(), flags));

		if(handle == QS_FILE_INVALID_HANDLE)
			throw std::runtime_error(std::strerror(errno));

		return File(handle);
	}

	SizeT File::size()
	{
		struct stat stats;

		if(fstat(toNativeHandle(handle_), &stats) == -1)
			throw std::runtime_error(std::strerror(errno));

		return stats.st_size;
	}

	SizeT File::read(void* buf, SizeT size)
	{
		SizeT bytesToRead = size;

		do
		{
			auto result = ::read(toNativeHandle(handle_), buf, bytesToRead);

			if(result == -1)
				throw std::runtime_error(std::strerror(errno));

			if(result == 0)
				return size - bytesToRead;

			auto res = static_cast<SizeT>(result);
			reinterpret_cast<char*&>(buf) += res;
			bytesToRead -= res;
		}

		while(bytesToRead != 0);

		return size;
	}

	void File::write(void const* buf, SizeT size)
	{
		do
		{
			auto result = ::write(toNativeHandle(handle_), buf, size);

			if(result == -1)
				throw std::runtime_error(std::strerror(errno));

			auto res = static_cast<SizeT>(result);
			size -= res;
			reinterpret_cast<char const*&>(buf) += res;
		}

		while(size != 0);
	}

	void File::truncate()
	{
		if(ftruncate(toNativeHandle(handle_), tell()) == -1)
			throw std::runtime_error(std::strerror(errno));
	}

	void File::flush()
	{
		if(fsync(toNativeHandle(handle_)) == -1)
			throw std::runtime_error(std::strerror(errno));
	}

	void File::seek(SizeT offset, SeekType type)
	{
		static constexpr int seekTypes[] = { SEEK_SET, SEEK_CUR, SEEK_END };

		if(lseek(toNativeHandle(handle_), boost::numeric_cast<off_t>(offset), seekTypes[static_cast<unsigned>(type)]) == -1)
			throw std::runtime_error(std::strerror(errno));
	}

	SizeT File::tell()
	{
		auto pos = lseek(toNativeHandle(handle_), 0, SEEK_CUR);

		if(pos == -1)
			throw std::runtime_error(std::strerror(errno));

		return pos;
	}

#endif
}
