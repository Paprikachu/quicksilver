// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <tuple>
#include <type_traits>

#include "Types.hpp"

namespace qs
{
	namespace detail
	{
		template <bool B>
		using BoolC = std::integral_constant<bool, B>;

		template <SizeT, typename Tuple, typename F, typename... Args>
		void tupleForEach(Tuple&, F&, std::false_type, Args&...)
		{}

		template <SizeT N, typename Tuple, typename F, typename... Args>
		void tupleForEach(Tuple& tuple, F& f, std::true_type, Args&... args)
		{
			f(std::get<N>(tuple), args...);
			tupleForEach<N + 1>(tuple, f, BoolC<(N + 1 < std::tuple_size<Tuple>::value)>(), args...);
		}
	}

	template <typename T>
	T& constifyIfRValue(T& value) { return value; }

	template <typename T>
	T const& constifyIfRValue(T const& value) { return value; }

	template <typename Tuple, typename F, typename... Args>
	void tupleForEach(Tuple& tuple, F f, Args&&... args)
	{
		// can't perfect forward args, passed to multiple functions
		detail::tupleForEach<0u>(tuple, f, detail::BoolC<std::tuple_size<Tuple>::value != 0>(), constifyIfRValue(std::forward<Args>(args))...);
	}
}
