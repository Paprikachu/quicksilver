// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <limits>
#include <ostream>

#include "PrefixPostfix.hpp"

namespace qs
{
#define QS_DEFINE_ASSIGNMENT_OP(op) \
	Integer& operator op (Integer other) \
	{ \
		value_ op other.value_; \
		return *this; \
	}

#define QS_DEFINE_UNARY_OP(op) \
	Integer& operator op QS_PREFIX \
	{ \
		op value_; \
		return *this; \
	} \
	\
	Integer operator op QS_POSTFIX \
	{ \
		auto tmp = *this; \
		op*this; \
		return tmp; \
	}

#define QS_DEFINE_BINARY_OP(op) \
	template <typename T, typename DomainTag> \
	Integer<T, DomainTag> operator op (Integer<T, DomainTag> lhs, Integer<T, DomainTag> rhs) \
	{ \
		return Integer<T, DomainTag>(lhs.value() op rhs.value()); \
	}

#define QS_DEFINE_COMPARISON_OP(op) \
	template <typename T, typename DomainTag> \
	bool operator op (Integer<T, DomainTag> lhs, Integer<T, DomainTag> rhs) \
	{ \
		return lhs.value() op rhs.value(); \
	}

	template <typename T, typename DomainTag>
	struct Integer
	{
		Integer() = default;
		Integer(Integer const&) = default;
		Integer& operator = (Integer const&) = default;
		~Integer() = default;

		explicit Integer(T value) : value_(value) {}

		void value(T value) { value_ = value; }
		T value() const { return value_; }

		QS_DEFINE_ASSIGNMENT_OP(+=)
		QS_DEFINE_ASSIGNMENT_OP(-=)
		QS_DEFINE_ASSIGNMENT_OP(*=)
		QS_DEFINE_ASSIGNMENT_OP(/=)
		QS_DEFINE_ASSIGNMENT_OP(%=)

		QS_DEFINE_ASSIGNMENT_OP(<<=)
		QS_DEFINE_ASSIGNMENT_OP(>>=)
		QS_DEFINE_ASSIGNMENT_OP(&=)
		QS_DEFINE_ASSIGNMENT_OP(|=)
		QS_DEFINE_ASSIGNMENT_OP(^=)

		QS_DEFINE_UNARY_OP(++)
		QS_DEFINE_UNARY_OP(--)

	private:
		T value_;
	};

	QS_DEFINE_BINARY_OP(+)
	QS_DEFINE_BINARY_OP(-)
	QS_DEFINE_BINARY_OP(*)
	QS_DEFINE_BINARY_OP(/)
	QS_DEFINE_BINARY_OP(%)

	QS_DEFINE_BINARY_OP(<<)
	QS_DEFINE_BINARY_OP(>>)
	QS_DEFINE_BINARY_OP(&)
	QS_DEFINE_BINARY_OP(|)
	QS_DEFINE_BINARY_OP(^)

	QS_DEFINE_COMPARISON_OP(==)
	QS_DEFINE_COMPARISON_OP(!=)
	QS_DEFINE_COMPARISON_OP(<)
	QS_DEFINE_COMPARISON_OP(>)
	QS_DEFINE_COMPARISON_OP(<=)
	QS_DEFINE_COMPARISON_OP(>=)

#undef QS_DEFINE_ASSIGNMENT_OP
#undef QS_DEFINE_UNARY_OP
#undef QS_DEFINE_BINARY_OP
#undef QS_DEFINE_COMPARISON_OP

	template <typename T, typename DomainTag>
	std::ostream& operator << (std::ostream& os, Integer<T, DomainTag> i)
	{
		return os << i.value();
	}
}

namespace std
{
	template <typename T, typename DomainTag>
	class numeric_limits<qs::Integer<T, DomainTag>>
	{
		using Int = qs::Integer<T, DomainTag>;

	public:
		static Int max() { return Int(numeric_limits<T>::max()); }
		static Int min() { return Int(numeric_limits<T>::min()); }
	};
}
