// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <utility>

namespace qs
{
	namespace detail
	{
		template <typename Init, typename F>
		auto foldl(Init&& init, F const&)
		{
			return std::forward<Init>(init);
		}

		template <typename Init, typename F, typename Head, typename... Tail>
		auto foldl(Init&& init, F const& f, Head&& head, Tail&&... tail)
		{
			return foldl(f(std::forward<Init>(init), std::forward<Head>(head)), f, std::forward<Tail>(tail)...);
		}

		template <typename Init, typename F>
		auto foldr(Init&& init, F const&)
		{
			return std::forward<Init>(init);
		}

		template <typename Init, typename F, typename Head, typename... Tail>
		auto foldr(Init&& init, F const& f, Head&& head, Tail&&... tail)
		{
			return f(std::forward<Head>(head), foldr(std::forward<Init>(init), f, std::forward<Tail>(tail)...));
		}
	}

	template <typename Init, typename BinFn, typename... Args>
	auto foldl(Init&& init, BinFn const& fn, Args&&... args)
	{
		return detail::foldl(std::forward<Init>(init), fn, std::forward<Args>(args)...);
	}

	template <typename Init, typename BinFn, typename... Args>
	auto foldr(Init&& init, BinFn const& fn, Args&&... args)
	{
		return detail::foldr(std::forward<Init>(init), fn, std::forward<Args>(args)...);
	}
}
