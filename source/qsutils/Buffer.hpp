// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>

#include "Types.hpp"

namespace qs
{
	struct Buffer
	{
		Buffer() = default;

		Buffer(Buffer&&) = default;
		Buffer& operator = (Buffer&&) = default;

		Buffer(Buffer const&) = delete;
		Buffer& operator = (Buffer const&) = delete;

		~Buffer() = default;

		UInt8* data() { return data_.get(); }
		UInt8 const* data() const { return data_.get(); }

		static Buffer allocate(SizeT size)
		{
			Buffer buffer;
			buffer.data_ = std::make_unique<UInt8[]>(size);
			return buffer;
		}

	private:
		std::unique_ptr<UInt8[]> data_;
	};
}
