// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <mutex>
#include <utility>

#include "Assertions.hpp"
#include "Buffer.hpp"
#include "Types.hpp"

namespace qs
{
	struct BufferManager
	{
		struct UniqueRef
		{
			UniqueRef(UniqueRef&&) = default;
			UniqueRef& operator = (UniqueRef&&) = default;

			UniqueRef() : mgr_(), buffer_() {}

			UniqueRef(BufferManager& mgr, Buffer&& buffer)
				: mgr_(&mgr), buffer_(std::move(buffer))
			{}

			~UniqueRef()
			{
				if(buffer_.data())
					mgr_->release(std::move(buffer_));
			}

			UInt8* data() { return buffer_.data(); }
			UInt8 const* data() const { return buffer_.data(); }

		private:
			BufferManager* mgr_;
			Buffer buffer_;
		};

		BufferManager(SizeT size)
			: bufferSize_(size)
		{
			QS_ASSERT(size >= sizeof(Buffer), "buffer size needs to be at least large enough to hold a buffer object")
		}

		BufferManager(BufferManager&& other)
			: list_(std::exchange(other.list_, Buffer()))
			, bufferSize_(other.bufferSize_)
		{}

		BufferManager& operator = (BufferManager other)
		{
			list_ = std::exchange(other.list_, Buffer());
			bufferSize_ = other.bufferSize_;
			return *this;
		}

		~BufferManager()
		{
			while(list_.data())
				popFront();
		}

		UniqueRef aquire()
		{
			std::unique_lock<std::mutex> lock(m_);

			if(list_.data())
				return UniqueRef(*this, popFront());

			lock.unlock();
			return UniqueRef(*this, Buffer::allocate(bufferSize_));
		}

		void release(Buffer&& buffer)
		{
			std::lock_guard<std::mutex> _(m_);
			pushFront(std::move(buffer));
		}

	private:
		void pushFront(Buffer&& buffer)
		{
			new (buffer.data()) Buffer(std::move(list_));
			list_ = std::move(buffer);
		}

		Buffer popFront()
		{
			auto& currentBuffer = *reinterpret_cast<Buffer*>(list_.data());
			auto nextFront = std::move(currentBuffer);
			currentBuffer.~Buffer();
			return std::exchange(list_, std::move(nextFront));
		}

		mutable std::mutex m_;
		Buffer list_;
		SizeT bufferSize_;
	};
}
