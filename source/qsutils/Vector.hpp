// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <numeric>
#include <utility>

#include "ImplicitCast.hpp"
#include "LambdaOperators.hpp"
#include "Types.hpp"
#include "UHash.hpp"
#include "Uninitialized.hpp"

namespace qs
{
	template <typename T, SizeT N, typename Derived>
	struct VectorBase
	{
		SizeT size() const { return N; }

		T* data() { return components_.data(); }
		T const* data() const { return components_.data(); }

		T* begin() { return data(); }
		T* end() { return data() + N; }

		T const* begin() const { return data(); }
		T const* end() const { return data() + N; }

		T& operator [] (SizeT index) { return components_[index]; }
		T operator [] (SizeT index) const { return components_[index]; }

		std::array<T, N> toArray() const { return components_; }

		T lengthSquared() const
		{
			auto addSquared = [](T sum, T next){ return sum + next * next; };
			return std::accumulate(begin(), end(), T(), addSquared);
		}

		template <typename U = T>
		U length() const
		{
			using std::sqrt;
			return sqrt(implicit_cast<U>(lengthSquared()));
		}

		Derived& operator += (VectorBase const& v);
		Derived& operator -= (VectorBase const& v);

		template <typename Scalar>
		Derived& operator *= (Scalar scalar);

		template <typename Scalar>
		Derived& operator /= (Scalar scalar);

		template <typename Scalar>
		Derived& operator %= (Scalar scalar);

	protected:
		VectorBase(Uninitialized) {}

		VectorBase(std::array<T, N> components)
			: components_(components)
		{}

	private:
		Derived& derivedThis() { return static_cast<Derived&>(*this); }

		std::array<T, N> components_;
	};

	template <typename T, SizeT N, typename Derived, typename F>
	Derived transform(VectorBase<T, N, Derived> const& v, F&& f)
	{
		Derived result(uninitialized);
		std::transform(v.begin(), v.end(), result.begin(), std::forward<F>(f));
		return result;
	}

	template <typename T, SizeT N, typename Derived, typename F>
	Derived transform(VectorBase<T, N, Derived> const& first, VectorBase<T, N, Derived> const& second, F&& f)
	{
		Derived result(uninitialized);
		std::transform(first.begin(), first.end(), second.begin(), result.begin(), std::forward<F>(f));
		return result;
	}

#define QS_DEFINE_VECTOR_ASSIGNMENT_OP(op) \
	template <typename T, SizeT N, typename Derived> \
	Derived& VectorBase<T, N, Derived>::operator op (VectorBase<T, N, Derived> const& v) \
	{ \
		*this = transform(*this, v, QS_INFIX_OP_FN(op)); \
		return derivedThis(); \
	}

	QS_DEFINE_VECTOR_ASSIGNMENT_OP(+=)
	QS_DEFINE_VECTOR_ASSIGNMENT_OP(-=)

#undef QS_DEFINE_VECTOR_ASSIGNMENT_OP

#define QS_DEFINE_SCALAR_ASSIGNMENT_OP(op) \
	template <typename T, SizeT N, typename Derived> \
	template <typename Scalar> \
	Derived& VectorBase<T, N, Derived>::operator op (Scalar scalar) \
	{ \
		auto withScalar = [scalar](T x){ return x op scalar; }; \
		*this = transform(*this, withScalar); \
		return derivedThis(); \
	}

	QS_DEFINE_SCALAR_ASSIGNMENT_OP(*=)
	QS_DEFINE_SCALAR_ASSIGNMENT_OP(/=)
	QS_DEFINE_SCALAR_ASSIGNMENT_OP(%=)

#undef QS_DEFINE_SCALAR_ASSIGNMENT_OP

#define QS_DEFINE_EQ_COMPARION(op) \
	template <typename T, SizeT N, typename Derived> \
	bool operator op (VectorBase<T, N, Derived> const& lhs, VectorBase<T, N, Derived> const& rhs) \
	{ \
		return lhs.toArray() op rhs.toArray(); \
	}

	QS_DEFINE_EQ_COMPARION(==)
	QS_DEFINE_EQ_COMPARION(!=)

#undef QS_DEFINE_EQ_COMPARION

#define QS_DEFINE_LT_COMPARION(op) \
	template <typename T, SizeT N, typename Derived> \
	bool operator op (VectorBase<T, N, Derived> const& lhs, VectorBase<T, N, Derived> const& rhs) \
	{ \
		return lhs.lengthSquared() op rhs.lengthSquared(); \
	}

	QS_DEFINE_LT_COMPARION(<)
	QS_DEFINE_LT_COMPARION(>)
	QS_DEFINE_LT_COMPARION(<=)
	QS_DEFINE_LT_COMPARION(>=)

#undef QS_DEFINE_LT_COMPARION

#define QS_DEFINE_SCALAR_OP(op) \
	template <typename T, SizeT N, typename Derived, typename Scalar> \
	Derived operator op (VectorBase<T, N, Derived> const& v, Scalar scalar) \
	{ \
		auto withScalar = [scalar](T x){ return x op scalar; }; \
		return transform(v, withScalar); \
	}

	QS_DEFINE_SCALAR_OP(*)
	QS_DEFINE_SCALAR_OP(/)
	QS_DEFINE_SCALAR_OP(%)

#undef QS_DEFINE_SCALAR_OP

#define QS_DEFINE_VECTOR_OP(op) \
	template <typename T, SizeT N, typename Derived> \
	Derived operator op (VectorBase<T, N, Derived> const& lhs, VectorBase<T, N, Derived> const& rhs) \
	{ \
		return transform(lhs, rhs, QS_INFIX_OP_FN(op)); \
	}

	QS_DEFINE_VECTOR_OP(+)
	QS_DEFINE_VECTOR_OP(-)

#undef QS_DEFINE_VECTOR_OP

	template <typename T, SizeT N, typename Derived>
	std::ostream& operator << (std::ostream& os, VectorBase<T, N, Derived> const& v)
	{
		os << "Vector(";

		if(N != 0)
		{
			os << v[0];

			for(SizeT i = 1; i != N; ++i)
				os << ',' << v[i];
		}

		return os << ')';
	}

	template <typename Hasher, typename T, SizeT N, typename Derived>
	void hashAppend(Hasher& hasher, VectorBase<T, N, Derived> const& v)
	{
		hashAppend(hasher, v.data(), v.size());
	}

	template <typename T, typename DomainTag>
	struct Vector2 : VectorBase<T, 2, Vector2<T, DomainTag>>
	{
		Vector2(Uninitialized) : Base(uninitialized) {}
		Vector2()              : Base({ T(), T() })  {}
		Vector2(T x, T y)      : Base({ x, y })      {}

		T x() const { return (*this)[0]; }
		T z() const { return (*this)[1]; }

		T& x() { return (*this)[0]; }
		T& z() { return (*this)[1]; }

	private:
		using Base = VectorBase<T, 2, Vector2<T, DomainTag>>;
	};

	template <typename T, typename DomainTag>
	struct Vector3 : VectorBase<T, 3, Vector3<T, DomainTag>>
	{
		Vector3(Uninitialized) : Base(uninitialized)     {}
		Vector3()              : Base({ T(), T(), T() }) {}
		Vector3(T x, T y, T z) : Base({ x, y, z })       {}

		T x() const { return (*this)[0]; }
		T y() const { return (*this)[1]; }
		T z() const { return (*this)[2]; }

		T& x() { return (*this)[0]; }
		T& y() { return (*this)[1]; }
		T& z() { return (*this)[2]; }

	private:
		using Base = VectorBase<T, 3, Vector3<T, DomainTag>>;
	};
}
