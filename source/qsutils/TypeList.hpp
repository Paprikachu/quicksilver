// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <type_traits>

#include "Identity.hpp"
#include "Types.hpp"

namespace qs
{
	template <typename...>
	struct TypeList
	{};

	template <typename>
	struct Size;

	template <typename... L>
	struct Size<TypeList<L...>>
		: std::integral_constant<SizeT, sizeof...(L)>
	{};

	template <typename, SizeT>
	struct At;

	template <typename Head, typename... Tail, SizeT Index>
	struct At<TypeList<Head, Tail...>, Index>
		: At<TypeList<Tail...>, Index - 1>
	{};

	template <typename Head, typename... Tail>
	struct At<TypeList<Head, Tail...>, 0>
		: Identity<Head>
	{};
}
