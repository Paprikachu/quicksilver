#include <boost/predef.h>

#if BOOST_OS_WINDOWS

#include <boost/locale/encoding_utf.hpp>

#include "Windows.hpp"

namespace qs
{
	QS_DLL_EXPORT
	std::string getLastError()
	{
		auto error = GetLastError();
		wchar_t buffer[1024];
		FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM, nullptr, error, LANG_SYSTEM_DEFAULT, buffer, sizeof buffer, nullptr);
		return narrow(buffer);
	}

	QS_DLL_EXPORT
	std::wstring widen(std::string const& str)
	{
		return boost::locale::conv::utf_to_utf<wchar_t>(str);
	}

	QS_DLL_EXPORT
	std::string narrow(std::wstring const& str)
	{
		return boost::locale::conv::utf_to_utf<char>(str);
	}
}

#endif
