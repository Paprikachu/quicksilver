// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstring>
#include <memory>
#include <type_traits>
#include <utility>

#include "Assertions.hpp"
#include "StrongTypedefs.hpp"
#include "Types.hpp"

namespace qs
{
	template <typename Key>
	struct DefaultIndexConverter
	{
		using IndexType = Key;

		static IndexType toIndex(Key key)
		{
			return key;
		}

		static Key fromIndex(IndexType index)
		{
			return index;
		}
	};

	template <typename T, typename DomainTag>
	struct DefaultIndexConverter<Integer<T, DomainTag>>
	{
		using IndexType = T;

		static IndexType toIndex(Integer<T, DomainTag> i)
		{
			return i.value();
		}

		static Integer<T, DomainTag> fromIndex(IndexType index)
		{
			return Integer<T, DomainTag>(index);
		}
	};

	float const ARRAYMAP_GROWTH_FACTOR = 1.5f;

	// holds keys and values in contiguous arrays of range [0, highest_key], allowing constant time access without hashing
	// entries with a key below the highest key always consume space, even if the (key, value) pair is not present
	// keys need to be of (positive) integer type as they are used as array indices, values can be of any type
	// resizing invalidates references but not iterators, moving invalidates iterators but not references
	// due to the mapping onto an array, the container is always sorted by the key automagically
	//
	// keys and values are stored in seperate arrays because:
	// 1. memory efficiency: vector<pair<Value, bool>> wastes 7 bits per bool, plus eventual padding
	// 2. cache efficiency: iteration is *fast*, because lots of flags fit in a cache line, values are only read if present
	// 3. potential simd optimizations from the compiler
	// ... etc.
	template
	<
		typename Key, typename Value,
		template <typename> class IndexConverter = DefaultIndexConverter
	>
	struct ArrayMap
	{
		using IndexType = typename IndexConverter<Key>::IndexType;

		static IndexType toIndex(Key key)
		{
			return IndexConverter<Key>::toIndex(key);
		}

		static Key fromIndex(IndexType index)
		{
			return IndexConverter<Key>::fromIndex(index);
		}

		ArrayMap() : present_(nullptr), values_(nullptr), size_(0) {}

		ArrayMap(ArrayMap const&) = delete;
		ArrayMap& operator = (ArrayMap const&) = delete;

		ArrayMap(ArrayMap&&) noexcept = default;
		ArrayMap& operator = (ArrayMap&&) noexcept = default;

		~ArrayMap() { destroy(); }

		template <typename... Args>
		std::pair<bool, Value*> emplace(Key key, Args&&... args)
		{
			auto k = toIndex(key);

			if(k >= size_)
				resize(static_cast<IndexType>(ARRAYMAP_GROWTH_FACTOR * (k + 1)));

			if(presentAt(k))
				return { false, &valueAt(k) };

			markPresent(k);
			return { true, new (&values_[k]) Value(std::forward<Args>(args)...) };
		}

		bool erase(Key key)
		{
			auto k = toIndex(key);

			if(k >= size_)
				return false;

			if(!presentAt(k))
				return false;

			markEmpty(k);
			valueAt(k).~Value();

			return true;
		}

		Value* find(Key key) { return const_cast<Value*>(static_cast<ArrayMap const*>(this)->find(key)); }

		Value const* find(Key key) const
		{
			auto k = toIndex(key);

			if(k >= size_)
				return nullptr;

			if(!presentAt(k))
				return nullptr;

			return &valueAt(k);
		}

		struct Iterator
		{
			friend ArrayMap;

			struct KV
			{
				KV(Key key, Value& value) : key(key), value(value) {}

				Key const key;
				Value& value;
			};

			Iterator(Iterator const&) = default;
			Iterator& operator = (Iterator const&) = default;
			~Iterator() = default;

			Iterator() : map_(nullptr), index_(0) {}
			Iterator(ArrayMap& map, IndexType index) : map_(&map), index_(index) { skipEmptyValues(); }

			Key key() const { return fromIndex(index_); }
			Value& value() const { return current(); }

			KV operator * () const { return KV(fromIndex(index_), current()); }

			Iterator& operator ++ () { ++index_; skipEmptyValues(); return *this; }
			Iterator operator ++ (int) { auto tmp = *this; ++*this; return tmp; }

			friend bool operator == (Iterator lhs, Iterator rhs)
			{
				QS_ASSERT(lhs.map_ == rhs.map_, "iterators of different containers compared")
				return lhs.index_ == rhs.index_;
			}

			friend bool operator != (Iterator lhs, Iterator rhs) { return !(lhs == rhs); }

		private:
			void skipEmptyValues()
			{
				while(index_ != map_->size_ && !map_->presentAt(index_))
					++index_;
			}

			Value& current() const
			{
				QS_ASSERT(map_ && index_ < map_->size_ && map_->presentAt(index_), "invalid iterator dereferenced")
				return map_->valueAt(index_);
			}

			ArrayMap* map_;
			IndexType index_;
		};

		struct ConstIterator
		{
			friend ArrayMap;

			struct KV
			{
				KV(Key key, Value const& value) : key(key), value(value) {}

				Key const key;
				Value const& value;
			};

			ConstIterator(ConstIterator const&) = default;
			ConstIterator& operator = (ConstIterator const&) = default;
			~ConstIterator() = default;

			ConstIterator() : map_(nullptr), index_(0) {}
			ConstIterator(ArrayMap const& map, IndexType index) : map_(&map), index_(index) { skipEmptyValues(); }
			ConstIterator(Iterator iter) : map_(iter.map_), index_(iter.index_) {}

			Key key() const { return fromIndex(index_); }
			Value const& value() const { return current(); }

			KV operator * () const { return KV(index_, current()); }

			ConstIterator& operator ++ () { ++index_; skipEmptyValues(); return *this; }
			ConstIterator operator ++ (int) { auto tmp = *this; ++*this; return tmp; }

			friend bool operator == (ConstIterator lhs, ConstIterator rhs)
			{
				QS_ASSERT(lhs.map_ == rhs.map_, "iterators of different containers compared")
				return lhs.index_ == rhs.index_;
			}

			friend bool operator != (ConstIterator lhs, ConstIterator rhs) { return !(lhs == rhs); }

		private:
			void skipEmptyValues()
			{
				while(index_ != map_->size_ && !map_->presentAt(index_))
					++index_;
			}

			Value const& current() const
			{
				QS_ASSERT(map_ && index_ < map_->size_ && map_->presentAt(index_), "invalid iterator dereferenced")
				return map_->valueAt(index_);
			}

			ArrayMap const* map_;
			IndexType index_;
		};

		Iterator begin() { return Iterator(*this, 0); }
		Iterator end() { return Iterator(*this, size_); }

		ConstIterator begin() const { return ConstIterator(*this, 0); }
		ConstIterator end() const { return ConstIterator(*this, size_); }

		ConstIterator cbegin() const { return begin(); }
		ConstIterator cend() const { return cend(); }

		Iterator upgrade(ConstIterator it) { return Iterator(*this, it.index_); }

	private:
		static IndexType presentIndex(IndexType index) { return index >> 3; }
		static UInt8 presentMask(IndexType index) { return 1 << (index & 7); }

		bool presentAt(IndexType index) const
		{
			return present_[presentIndex(index)] & presentMask(index);
		}

		void markPresent(IndexType index)
		{
			present_[presentIndex(index)] |= presentMask(index);
		}

		void markEmpty(IndexType index)
		{
			present_[presentIndex(index)] &= ~presentMask(index);
		}

		Value& valueAt(IndexType index) { return const_cast<Value&>(static_cast<ArrayMap const*>(this)->valueAt(index)); }
		Value const& valueAt(IndexType index) const { return *reinterpret_cast<Value const*>(&values_[index]); }

		void resize(IndexType size)
		{
			auto newBits = std::make_unique<UInt8[]>((size >> 3) + 1);
			auto newValues = std::make_unique<ValueStorage[]>(size);

			if(present_ != nullptr)
			{
				std::memcpy(newBits.get(), present_.get(), (size_ >> 3) + 1);

				for(IndexType key = 0; key != size_; ++key)
					if(presentAt(key))
						new (&newValues[key]) Value(std::move_if_noexcept(valueAt(key)));

				destroy();
			}

			present_ = std::move(newBits);
			values_ = std::move(newValues);
			size_ = size;
		}

		void destroy()
		{
			for(IndexType index = 0; index != size_; ++index)
				if(presentAt(index))
					valueAt(index).~Value();
		}

		using ValueStorage = typename std::aligned_storage<sizeof(Value), alignof(Value)>::type;

		std::unique_ptr<UInt8[]> present_;
		std::unique_ptr<ValueStorage[]> values_;
		IndexType size_;
	};
}
