// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <boost/predef.h>
#include <boost/preprocessor/cat.hpp>

// http://stackoverflow.com/questions/1113409/attribute-constructor-equivalent-in-vc
// http://stackoverflow.com/questions/7848790/imitating-constructor-of-static-objects-in-c

#if BOOST_COMP_MSVC

#define QS_STATIC_INIT \
	static void __cdecl BOOST_PP_CAT(staticInit, __LINE__)(); \
	__declspec(allocate(".CRT$XCU")) \
	void (__cdecl* BOOST_PP_CAT(staticInitFn, __LINE__))() = BOOST_PP_CAT(staticInit, __LINE__); \
	static void __cdecl BOOST_PP_CAT(staticInit, __LINE__)()
#else

#define QS_STATIC_INIT \
	__attribute__((constructor)) \
	static void BOOST_PP_CAT(staticInit, __LINE__)()

#endif
