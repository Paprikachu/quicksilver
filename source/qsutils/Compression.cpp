// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdexcept>

#include <boost/numeric/conversion/cast.hpp>

#include <zlib.h>

#include "Compression.hpp"

namespace qs
{
	QS_DLL_EXPORT
	std::vector<UInt8> zlibCompress(UInt8 const* data, SizeT size)
	{
		auto zlibSize = boost::numeric_cast<uLong>(size);

		z_stream stream = {};
		stream.avail_in = zlibSize;
		stream.next_in = const_cast<UInt8*>(data);

		if(deflateInit(&stream, Z_BEST_SPEED) != Z_OK)
			throw std::runtime_error("deflateInit failed");

		auto bufsize = deflateBound(&stream, zlibSize);

		std::vector<UInt8> buf(bufsize);

		stream.avail_out = bufsize;
		stream.next_out = buf.data();

		if(deflate(&stream, Z_FINISH) != Z_STREAM_END)
			throw std::runtime_error("error during compression");

		if(deflateEnd(&stream) != Z_OK)
			throw std::runtime_error("deflateEnd failed");

		buf.resize(buf.size() - stream.avail_out);

		return buf;
	}
}
