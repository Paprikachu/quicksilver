#include <gtest/gtest.h>

#include "core/protocol/Encoder.hpp"

using namespace qs;

TEST(EncoderTests, VarIntZero)
{
	UInt8 buffer[2] = { 42, 42 };

	UInt8* pbuf = buffer;
	encode(pbuf, varInt(0));

	EXPECT_EQ(buffer + 1, pbuf);
	EXPECT_EQ(0, buffer[0]);
	EXPECT_EQ(42, buffer[1]);
}

TEST(EncoderTests, VarIntPositive)
{
	UInt8 buffer[5] = { 42, 42, 42, 42, 42 };

	UInt8* pbuf = buffer;
	encode(pbuf, varInt(13371337)); // 00000000110011000000011111001001

	EXPECT_EQ(buffer + 4, pbuf);
	EXPECT_EQ(0b11001001, buffer[0]);
	EXPECT_EQ(0b10001111, buffer[1]);
	EXPECT_EQ(0b10110000, buffer[2]);
	EXPECT_EQ(0b00000110, buffer[3]);
	EXPECT_EQ(42, buffer[4]);
}

TEST(EncoderTests, VarIntNegative)
{
	UInt8 buffer[6] = { 42, 42, 42, 42, 42, 42 };

	UInt8* pbuf = buffer;
	encode(pbuf, varInt(-13371337)); // 11111111001100111111100000110111

	EXPECT_EQ(buffer + 5, pbuf);
	EXPECT_EQ(0b10110111, buffer[0]);
	EXPECT_EQ(0b11110000, buffer[1]);
	EXPECT_EQ(0b11001111, buffer[2]);
	EXPECT_EQ(0b11111001, buffer[3]);
	EXPECT_EQ(0b00001111, buffer[4]);
	EXPECT_EQ(42, buffer[5]);
}

TEST(EncoderTests, Int)
{
	UInt8 buffer[5] = { 42, 42, 42, 42, 42 };

	UInt8* pbuf = buffer;
	encode(pbuf, Int32(13));

	EXPECT_EQ(buffer + 4, pbuf);
	EXPECT_EQ(0, buffer[0]);
	EXPECT_EQ(0, buffer[1]);
	EXPECT_EQ(0, buffer[2]);
	EXPECT_EQ(13, buffer[3]);
	EXPECT_EQ(42, buffer[4]);
}

// TODO: float test

TEST(EncoderTests, FixedStringEmpty)
{
	UInt8 buffer[2] = { 82, 82 };

	UInt8* pbuf = buffer;
	encode(pbuf, FixedString<char, UInt8, 9>::fromCString(""));

	EXPECT_EQ(buffer + 1, pbuf);
	EXPECT_EQ(0, buffer[0]);
	EXPECT_EQ(82, buffer[1]);
}

TEST(EncoderTests, FixedStringNonEmpty)
{
	UInt8 buffer[6] = { 7, 7, 7, 7, 7, 7 };

	UInt8* pbuf = buffer;
	encode(pbuf, FixedString<char, UInt8, 20>::fromCString("test"));

	EXPECT_EQ(buffer + 5, pbuf);
	EXPECT_EQ(4, buffer[0]);
	EXPECT_EQ('t', buffer[1]);
	EXPECT_EQ('e', buffer[2]);
	EXPECT_EQ('s', buffer[3]);
	EXPECT_EQ('t', buffer[4]);
	EXPECT_EQ(7, buffer[5]);
}

TEST(EncoderTests, StdStringEmpty)
{
	UInt8 buffer[2] = { 99, 88 };

	UInt8* pbuf = buffer;
	encode(pbuf, std::string());

	EXPECT_EQ(buffer + 1, pbuf);
	EXPECT_EQ(0, buffer[0]);
	EXPECT_EQ(88, buffer[1]);
}

TEST(EncoderTests, StdStringNonEmpty)
{
	UInt8 buffer[6] = { 9, 9, 9, 9, 9, 9 };

	UInt8* pbuf = buffer;
	encode(pbuf, std::string("test"));

	EXPECT_EQ(buffer + 5, pbuf);
	EXPECT_EQ(4, buffer[0]);
	EXPECT_EQ('t', buffer[1]);
	EXPECT_EQ('e', buffer[2]);
	EXPECT_EQ('s', buffer[3]);
	EXPECT_EQ('t', buffer[4]);
	EXPECT_EQ(9, buffer[5]);
}

TEST(EncoderTests, Range)
{
	UInt8 buffer[5] = { 1, 2, 3, 4, 5 };

	UInt8 data[] = { 9, 8, 7, 6 };
	UInt8* pbuf = buffer;
	encode(pbuf, data, data + 4);

	EXPECT_EQ(buffer + 4, pbuf);
	EXPECT_EQ(9, buffer[0]);
	EXPECT_EQ(8, buffer[1]);
	EXPECT_EQ(7, buffer[2]);
	EXPECT_EQ(6, buffer[3]);
	EXPECT_EQ(5, buffer[4]);
}

TEST(EncoderTests, Position)
{
	UInt8 buffer[9] = { 0xFF, 0xFF, 0xFF, 0b11000000, 0b00000011, 0xFF, 0xFF, 0xFF, 42 };

	UInt8* pbuf = buffer;
	encode(pbuf, BlockCoord(0, 0xFFF, 0));

	EXPECT_EQ(buffer + 8, pbuf);
	EXPECT_EQ(0, buffer[0]);
	EXPECT_EQ(0, buffer[1]);
	EXPECT_EQ(0, buffer[2]);
	EXPECT_EQ(0b00111111, buffer[3]);
	EXPECT_EQ(0b11111100, buffer[4]);
	EXPECT_EQ(0, buffer[5]);
	EXPECT_EQ(0, buffer[6]);
	EXPECT_EQ(0, buffer[7]);
	EXPECT_EQ(42, buffer[8]);
}
