include_directories(.)

file(GLOB_RECURSE HEADERS ./*.hpp)
file(GLOB_RECURSE SOURCES ./*.cpp)

add_library(qsflatgen MODULE ${HEADERS} ${SOURCES})
set_target_properties(qsflatgen PROPERTIES PREFIX "")
qs_set_cxx_compiler_settings(qsflatgen)

install(TARGETS qsflatgen DESTINATION "./generators/")