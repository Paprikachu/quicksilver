// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <fstream>
#include <functional>
#include <map>
#include <queue>
#include <string>

#include <boost/asio/buffer.hpp>
#include <boost/asio/completion_condition.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>

#include <json/json.h>

#include "BufferedSocket.hpp"
#include "qsutils/JsonUtils.hpp"
#include "qsutils/Logging.hpp"
#include "qsutils/NamespaceShorthands.hpp"

namespace qs
{
	using namespace std::placeholders;

	struct Rcon
	{
		using CommandHandler = std::function<void(std::string, std::function<void(std::string const&)>)>;

		Rcon(ba::io_service& service, Json::Value const& config, CommandHandler commandHandler)
			: acceptor_(service, btcp::endpoint(
				findIp(config, "rcon.listen.address"),
				find<UInt16>(config, "rcon.listen.port")))
			, socket_(service)
			, commandHandler_(std::move(commandHandler))
		{
			std::ifstream is("rcon-motd.txt");

			if(is)
			{
				for(std::string line; std::getline(is, line);)
					motd_ += line + "\r\n";

				if(!is.eof())
				{
					motd_.clear();
					warning("failed to load rcon-motd.txt");
				}
			}

			else
				warning("failed to load rcon-motd.txt");

			startAccept();
		}

	private:
		struct ClientData
		{
			ClientData(btcp::socket socket) : socket(std::move(socket)) {}

			StreamBufferedSocket socket;
			std::queue<std::string> lines;
		};

		void onCrLfWritten(ClientData& client)
		{
			if(client.lines.empty())
				startLineRead(client);
			else
				startLineWrite(client);
		}

		void startCrLfWrite(ClientData& client)
		{
			client.lines.pop();
			client.socket.asyncWrite(ba::buffer("\r\n"), std::bind(&Rcon::onCrLfWritten, this, std::ref(client)));
		}

		void startLineWrite(ClientData& client)
		{
			client.socket.asyncWrite(ba::buffer(client.lines.front()), std::bind(&Rcon::startCrLfWrite, this, std::ref(client)));
		}

		void startLineRead(ClientData& client)
		{
			client.socket.asyncReadUntil('\n', std::bind(&Rcon::onLineRead, this, std::ref(client), _1));
		}

		void onLineRead(ClientData& client, bs::error_code ec)
		{
			if(ec)
			{
				info("rcon: lost connection from %s", client.socket.remoteEndpoint());
				clients_.erase(client.socket.remoteEndpoint());
				return;
			}

			std::istream is(&client.socket.buffer());
			std::string line;
			std::getline(is, line);

			while(!line.empty() && line.back() == '\r')
				line.resize(line.size() - 1);

			if(!line.empty())
			{
				info("rcon: %s: %s", client.socket.remoteEndpoint(), line);
				commandHandler_(std::move(line), [&](std::string const& line){ client.lines.push(line); });

				if(!client.lines.empty())
					startLineWrite(client);
				else
					startLineRead(client);
			}

			else
				startLineRead(client);
		}

		void motdWriteHandler(ClientData& client)
		{
			startLineRead(client);
		}

		void startMotdWrite(ClientData& client)
		{
			client.socket.asyncWrite(ba::buffer(motd_), std::bind(&Rcon::motdWriteHandler, this, std::ref(client)));
		}

		void handleClient(btcp::socket socket)
		{
			auto endpoint = socket.remote_endpoint();
			info("rcon: new client from %s", endpoint);
			auto& client = clients_[endpoint] = std::make_unique<ClientData>(std::move(socket));
			startMotdWrite(*client);
		}

		void acceptHandler(bs::error_code ec)
		{
			if(ec)
			{
				if(ec == ba::error::operation_aborted)
					return;

				throw bs::system_error(ec);
			}

			handleClient(std::move(socket_));
			startAccept();
		}

		void startAccept()
		{
			acceptor_.async_accept(socket_, std::bind(&Rcon::acceptHandler, this, _1));
		}

		std::map<btcp::endpoint, std::unique_ptr<ClientData>> clients_;

		btcp::acceptor acceptor_;
		btcp::socket socket_;

		std::string motd_;
		CommandHandler commandHandler_;
	};
}
