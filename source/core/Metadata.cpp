// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdexcept>

#include "protocol/Decoder.hpp"
#include "protocol/Encoder.hpp"
#include "protocol/WireLength.hpp"

namespace qs
{
	bool Metadata::insert(UInt8 key, MetadataValue const& value)
	{
		using Kv = decltype(entries_)::value_type;
		return entries_.insert(Kv{key, value}).second;
	}

	MetadataValue* Metadata::find(UInt8 key)
	{
		auto it = entries_.find(key);

		if(it == entries_.end())
			return nullptr;

		return &it->second;
	}

	bc::flat_map<UInt8, MetadataValue> const& Metadata::entries() const
	{
		return entries_;
	}

	namespace
	{
		struct WireLengthVisitor : boost::static_visitor<SizeT>
		{
			template <typename T>
			SizeT operator () (T x) const
			{
				return wireLength(x);
			}

			SizeT operator () (Rotation r) const
			{
				return wireLength(r.pitch) + wireLength(r.yaw) + wireLength(r.roll);
			}

			SizeT operator () (BlockCoord coord) const
			{
				return wireLength(coord.x()) + wireLength(coord.y()) + wireLength(coord.z());
			}
		};

		struct EncodeVisitor : boost::static_visitor<>
		{
			EncodeVisitor(UInt8*& buffer)
				: buffer_(&buffer)
			{}

			template <typename T>
			void operator () (T const& value) const
			{
				encode(*buffer_, value);
			}

			void operator () (Rotation r) const
			{
				encode(*buffer_, r.pitch);
				encode(*buffer_, r.yaw);
				encode(*buffer_, r.roll);
			}

			void operator () (BlockCoord coord) const
			{
				encode(*buffer_, coord.x());
				encode(*buffer_, coord.y());
				encode(*buffer_, coord.z());
			}

		private:
			UInt8** buffer_;
		};
	}

	bool decode(UInt8 const*& buffer, SizeT& size, Metadata& data)
	{
		(void)buffer;
		(void)size;
		(void)data;
		throw std::runtime_error("metadata decode nyi");
	}

	void encode(UInt8*& buffer, Metadata const& data)
	{
		auto visitor = EncodeVisitor(buffer);

		for(auto& kv : data.entries())
		{
			auto type = static_cast<UInt8>(kv.second.which());
			*buffer++ = kv.first | (type << 5);
			boost::apply_visitor(visitor, kv.second);
		}

		encode(buffer, UInt8(127));
	}

	SizeT wireLength(Metadata const& data)
	{
		SizeT size = 1;

		for(auto& kv : data.entries())
		{
			++size;
			size += boost::apply_visitor(WireLengthVisitor(), kv.second);
		}

		return size;
	}
}