// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>
#include <ostream>
#include <type_traits>

#include <boost/optional.hpp>
#include <boost/predef.h>
#include <boost/variant.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/container/flat_set.hpp>

#if BOOST_COMP_CLANG || BOOST_COMP_GNUC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wextra" // address of variable declared as 'register'
#endif
#include <boost/uuid/uuid.hpp>
#if BOOST_COMP_CLANG || BOOST_COMP_GNUC
#pragma GCC diagnostic pop
#endif

#include "qsutils/FixedString.hpp"
#include "qsutils/NamespaceShorthands.hpp"
#include "qsutils/StrongTypedefs.hpp"
#include "qsutils/Types.hpp"
#include "qsutils/UHash.hpp"
#include "qsutils/Unreachable.hpp"
#include "qsutils/Vector.hpp"

namespace qsapi {}

namespace qs
{
	using namespace qsapi;

	template <typename T, T V>
	using Constant = std::integral_constant<T, V>;

	using Block = UInt16;

	inline bool isBlock(UInt16 id) { return id < 256; }
	inline bool isBlock(Int16 id) { return id < 256; }

	struct ClientIdTag;
	using ClientId = Integer<UInt32, ClientIdTag>;

	using Uuid = boost::uuids::uuid;

	struct GlobalEntityIdTag;
	using GlobalEntityId = Integer<Int32, GlobalEntityIdTag>;

	struct WorldEntityIdTag;
	using WorldEntityId = Integer<UInt32, WorldEntityIdTag>;

	using PlayerName = FixedString<char, UInt8, 16>;

	struct WorldIdTag;
	using WorldId = Integer<UInt16, WorldIdTag>;

	struct ChunkCoordTag;
	using ChunkCoord = Vector2<Int32, ChunkCoordTag>;

	struct BlockCoordTag;
	using BlockCoord = Vector3<Int32, BlockCoordTag>;

	struct EntityCoordTag;
	using EntityCoord = Vector3<Float64, EntityCoordTag>;

	struct ChunkBlockCoordTag;
	using ChunkBlockCoord = Vector3<UInt8, ChunkBlockCoordTag>;

	struct FixedEntityCoordTag;
	using FixedEntityCoord = Vector3<Int32, FixedEntityCoordTag>;

	inline FixedEntityCoord toFixed(EntityCoord coord)
	{
		coord *= 32;

		return FixedEntityCoord
			(
				static_cast<Int32>(coord.x()),
				static_cast<Int32>(coord.y()),
				static_cast<Int32>(coord.z())
			);
	}

	inline EntityCoord fromFixed(FixedEntityCoord coord)
	{
		return EntityCoord(coord.x(), coord.y(), coord.z()) / 32.;
	}

	inline ChunkCoord toChunkCoord(BlockCoord coord)
	{
		return ChunkCoord
			(
				// this relies on signed right-shift shifting in sign-bits
				coord.x() >> 4,
				coord.z() >> 4
			);
	}

	inline BlockCoord toBlockCoord(EntityCoord coord)
	{
		return BlockCoord
			(
				static_cast<Int32>(coord.x()),
				static_cast<Int32>(coord.y()),
				static_cast<Int32>(coord.z())
			);
	}

	inline ChunkCoord toChunkCoord(EntityCoord coord)
	{
		return toChunkCoord(toBlockCoord(coord));
	}

	inline ChunkBlockCoord toChunkBlockCoord(BlockCoord coord)
	{
		QS_ASSERT(coord.y() >= 0 && coord.y() <= 255, "invalid coord %s cannot be converted", coord)

		return ChunkBlockCoord
			(
				coord.x() & 0xF,
				static_cast<UInt8>(coord.y()),
				coord.z() & 0xF
			);
	}

	struct EntityLook
	{
		EntityLook() : components_{} {}
		EntityLook(Float32 yaw, Float32 pitch) : components_({yaw, pitch}) {}

		Float32& yaw() { return components_[0]; }
		Float32& pitch() { return components_[1]; }

		Float32 yaw() const { return components_[0]; }
		Float32 pitch() const { return components_[1]; }

	private:
		std::array<Float32, 2> components_;
	};

	enum struct PositionLookFlags : UInt8
	{
		NONE           =  0,
		RELATIVE_X     =  1,
		RELATIVE_Y     =  2,
		RELATIVE_Z     =  4,
		RELATIVE_YAW   =  8,
		RELATIVE_PITCH = 16,
	};

	inline PositionLookFlags operator | (PositionLookFlags lhs, PositionLookFlags rhs)
	{
		using UT = std::underlying_type<PositionLookFlags>::type;
		return static_cast<PositionLookFlags>(static_cast<UT>(lhs) | static_cast<UT>(rhs));
	}

	enum struct Difficulty : UInt8
	{
		PEACEFUL,
		EASY,
		NORMAL,
		HARD,
	};

	enum struct Dimension : Int8
	{
		NETHER    = -1,
		OVERWORLD = 0,
		END       = 1,
	};

	enum struct Gamemode : UInt8
	{
		SURVIVAL  = 0,
		CREATIVE  = 1,
		ADVENTURE = 2,
	};

	enum struct LevelType : SizeT
	{
		DEFAULT,
		FLAT,
		LARGE_BIOMES,
		AMPLIFIED,
		DEFAULT_1_1,
	};

	inline std::string toString(LevelType type)
	{
		static std::string levelTypes[] = { "default", "flat", "largeBiomes", "amplified", "default_1_1" };
		return levelTypes[static_cast<std::underlying_type<LevelType>::type>(type)];
	}

	enum struct AbilityFlags : UInt8
	{
		NONE     = 0,
		CREATIVE = 1,
		FLYING   = 2,
		CANFLY   = 4,
		GODMODE  = 8,
	};

	inline AbilityFlags operator | (AbilityFlags lhs, AbilityFlags rhs)
	{
		using UT = std::underlying_type<AbilityFlags>::type;
		return static_cast<AbilityFlags>(static_cast<UT>(lhs) | static_cast<UT>(rhs));
	}

	enum struct ChatType : Int8
	{
		CHAT_MESSAGE,
		SYSTEM_MESSAGE,
		ACTIONBAR_MESSAGE,
	};

	enum struct DigStatus : Int8
	{
		DIGGING_STARTED,
		DIGGING_CANCELLED,
		DIGGING_FINISHED,
		ITEM_DROP_STACK,
		ITEM_DROP,
		SHOOT_ARROW_OR_FINISH_EATING,
	};

	enum struct BlockFace : Int8
	{
		Y_NEG,
		Y_POS,
		Z_NEG,
		Z_POS,
		X_NEG,
		X_POS,

		INVALID = -1,
	};

	inline BlockCoord relative(BlockCoord coord, BlockFace face)
	{
		switch(face)
		{
		case BlockFace::Y_NEG: return coord + BlockCoord(0, -1, 0);
		case BlockFace::Y_POS: return coord + BlockCoord(0, 1, 0);
		case BlockFace::Z_NEG: return coord + BlockCoord(0, 0, -1);
		case BlockFace::Z_POS: return coord + BlockCoord(0, 0, 1);
		case BlockFace::X_NEG: return coord + BlockCoord(-1, 0, 0);
		case BlockFace::X_POS: return coord + BlockCoord(1, 0, 0);
		default: break;
		}

		QS_UNREACHABLE("invalid block face")
	}

	struct Rotation
	{
		Float32 pitch, yaw, roll;
	};

	struct NbtEmptyValue {};

	struct Nbt;

	// http://wiki.vg/NBT
	using NbtValue =
		boost::make_recursive_variant
		<
		NbtEmptyValue, // not official nbt, "default constructed nbt value"
		Int8,
		Int16,
		Int32,
		Int64,
		Float32,
		Float64,
		std::string,
		std::vector<Int8>,
		std::vector<Int32>,
		std::vector<boost::recursive_variant_>,
		bc::flat_set<Nbt>
		>::type;

	struct Nbt
	{
		NbtValue value;
		std::string name;
	};

	inline bool operator < (Nbt const& lhs, Nbt const& rhs)
	{
		return lhs.name < rhs.name;
	}

	struct SlotData
	{
		SlotData()
			: data(), type(0), damage(0), count(0)
		{}

		boost::optional<Nbt> data;
		Int16 type;
		Int16 damage;
		Int8 count;
	};

	using MetadataValue =
		boost::variant
		<
		Int8,
		Int16,
		Int32,
		Float32,
		std::string,
		SlotData,
		BlockCoord,
		Rotation
		>;

	struct Metadata
	{
		bool insert(UInt8 key, MetadataValue const& value);
		MetadataValue* find(UInt8 key);
		bc::flat_map<UInt8, MetadataValue> const& entries() const;

	private:
		bc::flat_map<UInt8, MetadataValue> entries_;
	};
}
