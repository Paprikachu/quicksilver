// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "GameTypes.hpp"
#include "entity/EntityPtr.hpp"
#include "protocol/Packets.hpp"

namespace qs
{
	struct Player;

	struct WorldTickEvent
	{
	};

	struct PlayerWorldEnterEvent
	{
		EntityPtr<Player> player;
	};

	struct PlayerWorldLeaveEvent
	{
		EntityPtr<Player> player;
	};

	struct NetworkPacketEvent
	{
		EntityPtr<Player> player;
		Packet packet;
	};

	struct PlayerMoveEvent
	{
		EntityPtr<Player> player;
		EntityCoord from, to;
	};

	struct PlayerBlockBreakEvent
	{
		EntityPtr<Player> player;
		BlockCoord location;
		Chunk* chunk;
		Block type;
	};

	struct PlayerBlockPlaceEvent
	{
		EntityPtr<Player> player;
		BlockCoord location;
		Chunk* chunk;
		Block type;
	};
}
