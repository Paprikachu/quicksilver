// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "qsutils/TypeList.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct HandlerBase
	{
		virtual ~HandlerBase() = default;

	protected:
		HandlerBase() = default;

		HandlerBase(HandlerBase const&) = default;
		HandlerBase(HandlerBase&&) = default;

		HandlerBase& operator = (HandlerBase const&) = default;
		HandlerBase& operator = (HandlerBase&&) = default;
	};

	struct World;

	template <typename Event>
	struct HandlerImpl
	{
		virtual void handle(World&, Event const& event) = 0;

	protected:
		~HandlerImpl() = default;
	};

	template <typename... E>
	struct Handler : HandlerBase, HandlerImpl<E>...
	{
		using Events = TypeList<E...>;
	};
}
