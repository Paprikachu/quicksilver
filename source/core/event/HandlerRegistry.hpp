// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <map>
#include <memory>
#include <typeinfo>
#include <utility>
#include <vector>

#include <boost/container/stable_vector.hpp>

#include "event/Handler.hpp"
#include "qsutils/Identity.hpp"
#include "qsutils/NamespaceShorthands.hpp"
#include "qsutils/PtrCompare.hpp"
#include "qsutils/TypeList.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct HandlerRegistry
	{
		template <typename Handler>
		void registerHandler(std::unique_ptr<Handler> handler)
		{
			registerHandlerImpl(std::move(handler), std::make_index_sequence<Size<typename Handler::Events>::type::value>());
		}

		template <typename Handler>
		void unregisterHandler(Handler& handler)
		{
			auto it = handlers_.find(&handler);

			if(it != handlers_.end())
			{
				for(auto& token : it->second)
					token.container.erase(token.iterator);

				handlers_.erase(it);
				handlersByType_.erase(&typeid(Handler));
			}
		}

		template <typename Handler>
		Handler* findHandler()
		{
			auto it = handlersByType_.find(&typeid(Handler));

			if(it == handlersByType_.end())
				return nullptr;

			return static_cast<Handler*>(it->second);
		}

		template <typename Event>
		void dispatch(World& world, Event& event)
		{
			auto it = handlersByEventType_.find(&typeid(Event));

			if(it != handlersByEventType_.end())
				for(auto handler : it->second)
					static_cast<HandlerImpl<Event>*>(handler)->handle(world, event);
		}

	private:
		struct RemovalToken
		{
			bc::stable_vector<void*>* container;
			bc::stable_vector<void*>::iterator iterator;
		};

		template <typename Handler, SizeT... I>
		void registerHandlerImpl(std::unique_ptr<Handler> handler, std::index_sequence<I...>)
		{
			bc::stable_vector<void*>* p;

			std::vector<RemovalToken> removalTokens =
			{
				{
					p = &handlersByEventType_[&typeid(typename At<typename Handler::Events, I>::Type)],
					p->insert(p->end(), static_cast<HandlerImpl<typename At<typename Handler::Events, I>::Type>*>(handler.get()))
				}...
			};

			handlersByType_.insert({ &typeid(Handler), handler.get() });
			handlers_[std::move(handler)] = std::move(removalTokens);
		}

		std::map<std::unique_ptr<HandlerBase>, std::vector<RemovalToken>, PtrCompare<HandlerBase>> handlers_;
		std::map<std::type_info const*, bc::stable_vector<void*>> handlersByEventType_;
		std::map<std::type_info const*, HandlerBase*> handlersByType_;
	};
}
