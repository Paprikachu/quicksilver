// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <type_traits>

#include "event/HandlerRegistry.hpp"
#include "qsutils/StaticInit.hpp"

namespace qs
{
	struct World;
	using HandlerFactory = void (*) (HandlerRegistry&, World&);

	void registerHandlerFactory(HandlerFactory factory);
	HandlerRegistry createHandlers(World& world);

	namespace detail
	{
		template <typename Handler>
		void registerHandlerImpl(HandlerRegistry& registry, World& world, std::true_type)
		{
			registry.registerHandler(std::make_unique<Handler>(world));
		}

		template <typename Handler>
		void registerHandlerImpl(HandlerRegistry& registry, World&, std::false_type)
		{
			registry.registerHandler(std::make_unique<Handler>());
		}

		template <typename Handler>
		void registerHandler(HandlerRegistry& registry, World& world)
		{
			registerHandlerImpl<Handler>(registry, world, std::integral_constant<bool, std::is_constructible<Handler, World&>::value>());
		}
	}
}

#define QS_REGISTER_HANDLER(...) \
	QS_STATIC_INIT \
	{ \
		qs::registerHandlerFactory( \
			[](qs::HandlerRegistry& registry, qs::World& world) \
			{ \
				qs::detail::registerHandler<__VA_ARGS__>(registry, world); \
			}); \
	}
