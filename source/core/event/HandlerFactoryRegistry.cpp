// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <vector>

#include "event/HandlerFactoryRegistry.hpp"

namespace qs
{
	namespace
	{
		std::vector<HandlerFactory>& factories()
		{
			static std::vector<HandlerFactory> facs;
			return facs;
		}
	}

	void registerHandlerFactory(HandlerFactory factory)
	{
		factories().push_back(factory);
	}

	HandlerRegistry createHandlers(World& world)
	{
		HandlerRegistry registry;

		for(auto& factory : factories())
			factory(registry, world);

		return registry;
	}
}
