// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "MinecraftServer.hpp"
#include "World.hpp"
#include "entity/Player.hpp"
#include "event/Events.hpp"
#include "event/Handler.hpp"
#include "event/HandlerFactoryRegistry.hpp"
#include "protocol/Packets.hpp"

namespace qs
{
	struct MoveHandler : Handler<NetworkPacketEvent>
	{
	private:
		virtual void handle(World& world, NetworkPacketEvent const& event) override final
		{
			if(event.packet.header.type == clientPayloadIdOf<PacketPlayerPosition>())
			{
				PacketPlayerPosition position;

				if(!decodePayload(event.packet, position))
				{
					world.server().disconnectWithMessage(event.player->cid(), "invalid packet PlayerPosition");
					return;
				}

				auto player = event.player;

				PlayerMoveEvent moveEvent;
				moveEvent.player = player;
				moveEvent.from = player->coord();
				moveEvent.to = position.coord;

				player->coord(position.coord);
				player->onGround(position.onGround);

				world.dispatchEvent(moveEvent);
			}
		}
	};
}

QS_REGISTER_HANDLER(qs::MoveHandler)
