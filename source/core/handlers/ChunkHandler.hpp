// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "GameTypes.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct Chunk;
	struct World;

	constexpr Int32 VIEW_DISTANCE_RADIUS_MAX = 16;
	constexpr Int32 VIEW_DISTANCE_DIAMETER_MAX = 2 * VIEW_DISTANCE_RADIUS_MAX + 1;

	Chunk* findChunkAt(World& world, ChunkCoord coord);
	Chunk& findOrGenerateChunkAt(World& world, ChunkCoord coord);
	Chunk& findOrCreateChunkAt(World& world, ChunkCoord coord);
}
