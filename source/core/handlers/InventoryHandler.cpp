// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "entity/EntityPtr.hpp"
#include "entity/Player.hpp"
#include "event/Events.hpp"
#include "event/Handler.hpp"
#include "event/HandlerFactoryRegistry.hpp"
#include "MinecraftServer.hpp"
#include "World.hpp"

namespace qs
{
	struct InventoryHandler : Handler<NetworkPacketEvent>
	{
	private:
		virtual void handle(World& world, NetworkPacketEvent const& event) override final
		{
			if(event.packet.header.type == clientPayloadIdOf<PacketCreativeInventoryAction>())
			{
				PacketCreativeInventoryAction packet;

				if(!decodePayload(event.packet, packet))
				{
					world.server().disconnectWithMessage(event.player->cid(), "malformed packet CreativeInventoryAction");
					return;
				}

				handle(world, event.player, packet);
			}

			else if(event.packet.header.type == clientPayloadIdOf<PacketHeldItemChange>())
			{
				PacketHeldItemChange packet;

				if(!decodePayload(event.packet, packet))
				{
					world.server().disconnectWithMessage(event.player->cid(), "malformed packet HeldItemChange");
					return;
				}

				handle(world, event.player, packet);
			}
		}

		void handle(World& world, EntityPtr<Player> player, PacketCreativeInventoryAction const& packet)
		{
			auto slot = static_cast<UInt16>(packet.slot);

			if(packet.slot < 0 || slot >= Inventory::SLOT_COUNT)
			{
				world.server().disconnectWithMessage(player->cid(), "invalid inventory slot id in CreativeInventoryAction");
				return;
			}

			if(packet.item.type == -1)
				player->inventory()[slot] = SlotData();
			else
				player->inventory()[slot] = packet.item;
		}

		void handle(World& world, EntityPtr<Player> player, PacketHeldItemChange const& packet)
		{
			if(packet.slot < 0 || packet.slot > 8)
			{
				world.server().disconnectWithMessage(player->cid(), "invalid inventory slot id in HeldItemChange");
				return;
			}

			auto slot = static_cast<UInt16>(packet.slot);
			player->inventory().selectedIndex(Inventory::FIRST_SELECTABLE_SLOT + slot);
		}
	};
}

QS_REGISTER_HANDLER(qs::InventoryHandler)