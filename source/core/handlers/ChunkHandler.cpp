// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <memory>
#include <string>
#include <unordered_map>
#include <utility>

#include <boost/numeric/conversion/cast.hpp>

#include <json/json.h>

#include "Chunk.hpp"
#include "ChunkGenerator.hpp"
#include "ChunkHandler.hpp"
#include "GameTypes.hpp"
#include "World.hpp"
#include "event/Events.hpp"
#include "event/Handler.hpp"
#include "event/HandlerFactoryRegistry.hpp"
#include "protocol/Packets.hpp"
#include "qsutils/Assertions.hpp"
#include "qsutils/Fnv1aHasher.hpp"
#include "qsutils/JsonUtils.hpp"
#include "qsutils/UHash.hpp"

namespace qs
{
	namespace
	{
		SizeT chunkDifference(ChunkCoord center1, ChunkCoord center2, Int32 radius, ChunkCoord* out)
		{
			auto xa = center2.x() - radius;
			auto xb = center2.x() + radius;
			auto za = center2.z() - radius;
			auto zb = center2.z() + radius;

			SizeT count = 0;

			for(auto x = center1.x() - radius; x != center1.x() + radius + 1; ++x)
			for(auto z = center1.z() - radius; z != center1.z() + radius + 1; ++z)
			{
				if(x < xa || x > xb || z < za || z > zb)
					out[count++] = {x, z};
			}

			return count;
		}
	}

	struct ChunkHandler : Handler<PlayerWorldEnterEvent, PlayerWorldLeaveEvent, PlayerMoveEvent>
	{
		ChunkHandler(World& world)
			: generator_(world,
				find<std::string>(world.config(), "generator.name"),
				find<UInt64>(world.config(), "generator.seed"),
				Json::FastWriter().write(resolvePath(world.config(), "generator.parameters")))
		{}

		Chunk* findChunkAt(ChunkCoord coord)
		{
			auto it = chunks_.find(coord);
			return it == chunks_.end() ? nullptr : it->second.get();
		}

		Chunk& findOrGenerateChunkAt(ChunkCoord coord)
		{
			auto chunk = findChunkAt(coord);

			if(chunk)
				return *chunk;

			return generateChunkAt(coord);
		}

		Chunk& findOrCreateChunkAt(ChunkCoord coord)
		{
			auto& chunk = findOrGenerateChunkAt(coord);

			if(!chunk.populated())
				generator_.populate(chunk);

			return chunk;
		}

		virtual void handle(World& world, PlayerWorldEnterEvent const& event) override final
		{
			auto centerChunkCoord = toChunkCoord(event.player->coord());

			PacketChunkData chunkData;
			chunkData.continuous = true;
			chunkData.skyLightNotSent = world.settings().dimension == Dimension::OVERWORLD;

			auto queueChunk =
				[this, &world, &chunkData, &event](ChunkCoord coord)
				{
					auto& chunk = findOrCreateChunkAt(coord);

					chunk.subscribe(event.player);

					chunkData.chunk = &chunk;
					chunkData.coord = coord;
					chunkData.sectionMask = chunk.sectionBitmap();
					chunkData.dataLength = boost::numeric_cast<Int32>(wireLength(chunk, chunkData.skyLightNotSent));

					world.netHandler().queueHugePacket(event.player->cid(), chunkData);
				};

			queueChunk(centerChunkCoord);

			// send chunks in circles of increasing radii
			for(Int32 radius = 1; radius != VIEW_DISTANCE_RADIUS_MAX + 1; ++radius)
			{
				for(Int32 x = -radius; x != radius; ++x)
					queueChunk({centerChunkCoord.x() + x, centerChunkCoord.z() - radius});

				for(Int32 z = -radius; z != radius; ++z)
					queueChunk({centerChunkCoord.x() + radius, centerChunkCoord.z() + z});

				for(Int32 x = radius; x != -radius; --x)
					queueChunk({centerChunkCoord.x() + x, centerChunkCoord.z() + radius});

				for(Int32 z = radius; z != -radius; --z)
					queueChunk({centerChunkCoord.x() - radius, centerChunkCoord.z() + z});
			}

			findChunkAt(centerChunkCoord)->enter(event.player);
		}

		virtual void handle(World&, PlayerWorldLeaveEvent const& event) override final
		{
			auto centerChunkCoord = toChunkCoord(event.player->coord());

			for(auto x = -VIEW_DISTANCE_RADIUS_MAX; x != VIEW_DISTANCE_RADIUS_MAX + 1; ++x)
			for(auto z = -VIEW_DISTANCE_RADIUS_MAX; z != VIEW_DISTANCE_RADIUS_MAX + 1; ++z)
				findChunkAt(centerChunkCoord + ChunkCoord{x, z})->unsubscribe(event.player);

			findChunkAt(centerChunkCoord)->leave(event.player);
		}

		virtual void handle(World& world, PlayerMoveEvent const& event) override final
		{
			auto from = toChunkCoord(event.from);
			auto to = toChunkCoord(event.to);

			if(from != to)
			{
				ChunkCoord chunksToSend[VIEW_DISTANCE_DIAMETER_MAX * VIEW_DISTANCE_DIAMETER_MAX];
				auto sendCount = chunkDifference(to, from, VIEW_DISTANCE_RADIUS_MAX, chunksToSend);

				ChunkCoord chunksToUnload[VIEW_DISTANCE_DIAMETER_MAX * VIEW_DISTANCE_DIAMETER_MAX];
				auto unloadCount = chunkDifference(from, to, VIEW_DISTANCE_RADIUS_MAX, chunksToUnload);

				auto skyLight = world.settings().dimension == Dimension::OVERWORLD;

				for(SizeT i = 0; i != sendCount; ++i)
				{
					auto coord = chunksToSend[i];
					auto& chunk = findOrCreateChunkAt(coord);

					chunk.subscribe(event.player);

					PacketChunkData chunkData;
					chunkData.continuous = true;
					chunkData.coord = coord;
					chunkData.sectionMask = chunk.sectionBitmap();
					chunkData.chunk = &chunk;
					chunkData.dataLength = boost::numeric_cast<Int32>(wireLength(chunk, skyLight));
					chunkData.skyLightNotSent = skyLight;

					world.netHandler().queueHugePacket(event.player->cid(), chunkData);
				}

				for(SizeT i = 0; i != unloadCount; ++i)
				{
					auto coord = chunksToUnload[i];
					findChunkAt(coord)->unsubscribe(event.player);

					PacketChunkData chunkData;
					chunkData.continuous = true;
					chunkData.dataLength = 0;
					chunkData.sectionMask = 0;
					chunkData.skyLightNotSent = skyLight;
					chunkData.coord = coord;
					chunkData.chunk = nullptr;

					world.netHandler().queuePacket(event.player->cid(), chunkData);
				}

				findChunkAt(from)->leave(event.player);
				findChunkAt(to)->enter(event.player);
			}
		}

	private:
		Chunk& generateChunkAt(ChunkCoord coord)
		{
			auto chunk = std::make_unique<Chunk>(coord);
			generator_.generate(*chunk);
			auto pair = chunks_.insert(std::make_pair(coord, std::move(chunk)));
			return *pair.first->second;
		}

		ChunkGenerator generator_;
		std::unordered_map<ChunkCoord, std::unique_ptr<Chunk>, UHash<Fnv1aHasher64>> chunks_;
	};

	Chunk* findChunkAt(World& world, ChunkCoord coord)
	{
		return world.handlers().findHandler<ChunkHandler>()->findChunkAt(coord);
	}

	Chunk& findOrGenerateChunkAt(World& world, ChunkCoord coord)
	{
		return world.handlers().findHandler<ChunkHandler>()->findOrGenerateChunkAt(coord);
	}

	Chunk& findOrCreateChunkAt(World& world, ChunkCoord coord)
	{
		return world.handlers().findHandler<ChunkHandler>()->findOrCreateChunkAt(coord);
	}
}

QS_REGISTER_HANDLER(qs::ChunkHandler)
