// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "GameTypes.hpp"
#include "MinecraftServer.hpp"
#include "World.hpp"
#include "api/GeneratorApi.hpp"
#include "entity/Player.hpp"
#include "event/Events.hpp"
#include "event/Handler.hpp"
#include "event/HandlerFactoryRegistry.hpp"
#include "handlers/ChunkHandler.hpp"
#include "protocol/Packets.hpp"
#include "qsutils/Assertions.hpp"

namespace qs
{
	struct BlockHandler : Handler<NetworkPacketEvent, PlayerBlockBreakEvent, PlayerBlockPlaceEvent>
	{
	private:
		void handleDigging(World& world, EntityPtr<Player> player, PacketPlayerDigging const& packet)
		{
			switch(packet.status)
			{
			case DigStatus::DIGGING_STARTED:
				{
					auto chunkCoord = toChunkCoord(packet.location);
					auto chunk = findChunkAt(world, chunkCoord);
					QS_ASSERT(chunk, "chunk %s not found", chunkCoord)

					auto blockCoord = toChunkBlockCoord(packet.location);
					auto& block = chunk->blockAt(blockCoord);
					auto oldType = block;
					block = AIR;

					PlayerBlockBreakEvent event;
					event.location = packet.location;
					event.chunk = chunk;
					event.player = player;
					event.type = oldType;
					world.dispatchEvent(event);
				}
			break;

			case DigStatus::DIGGING_CANCELLED:
			case DigStatus::DIGGING_FINISHED:
				// not used in creative
				break;

			default:
				// handled elsewhere
				break;
			}
		}

		void handleBlockPlacement(World& world, EntityPtr<Player> player, PacketPlayerBlockPlacement const& packet)
		{
			if(packet.face == BlockFace::INVALID)
				return;

			if(packet.cursorX == -1 && packet.cursorY == 255 && packet.cursorZ == -1)
				return;
			
			if(!isBlock(packet.slot.type))
				return;

			auto location = relative(packet.location, packet.face);

			auto chunkCoord = toChunkCoord(location);
			auto chunk = findChunkAt(world, chunkCoord);
			QS_ASSERT(chunk, "chunk %s not found", chunkCoord)

			auto blockCoord = toChunkBlockCoord(location);
			auto& block = chunk->blockAt(blockCoord);

			if(block != AIR)
				return;

			auto slot = player->inventory().selectedSlot();
			auto type = slot.type << 4 | slot.damage;

			block = type;

			PlayerBlockPlaceEvent event;
			event.player = player;
			event.chunk = chunk;
			event.location = location;
			event.type = type;
			world.dispatchEvent(event);
		}

		virtual void handle(World& world, NetworkPacketEvent const& event) override final
		{
			if(event.packet.header.type == clientPayloadIdOf<PacketPlayerDigging>())
			{
				PacketPlayerDigging packet;

				if(!decodePayload(event.packet, packet))
				{
					world.server().disconnectWithMessage(event.player->cid(), "invalid packet PlayerDigging");
					return;
				}

				handleDigging(world, event.player, packet);
			}

			else if(event.packet.header.type == clientPayloadIdOf<PacketPlayerBlockPlacement>())
			{
				PacketPlayerBlockPlacement packet;

				if(!decodePayload(event.packet, packet))
				{
					world.server().disconnectWithMessage(event.player->cid(), "invalid packet PlayerBlockPlacement");
					return;
				}

				handleBlockPlacement(world, event.player, packet);
			}
		}

		virtual void handle(World& world, PlayerBlockBreakEvent const& event) override final
		{
			PacketBlockChange packet;
			packet.location = event.location;
			packet.type = qsapi::AIR;

			for(auto player : event.chunk->subscribers())
				if(player != event.player)
					world.netHandler().queuePacket(player->cid(), packet);
		}

		virtual void handle(World& world, PlayerBlockPlaceEvent const& event) override final
		{
			PacketBlockChange packet;
			packet.location = event.location;
			packet.type = event.type;

			for(auto player : event.chunk->subscribers())
				if(player != event.player)
					world.netHandler().queuePacket(player->cid(), packet);
		}
	};
}

QS_REGISTER_HANDLER(qs::BlockHandler)