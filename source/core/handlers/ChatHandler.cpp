// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ChatHandler.hpp"
#include "GameTypes.hpp"
#include "MinecraftServer.hpp"
#include "World.hpp"
#include "event/Handler.hpp"
#include "event/HandlerFactoryRegistry.hpp"
#include "protocol/Packets.hpp"
#include "qsutils/Logging.hpp"

namespace qs
{
	struct ChatHandler : Handler<PlayerWorldEnterEvent, PlayerWorldLeaveEvent, NetworkPacketEvent>
	{
		virtual void handle(World& world, PlayerWorldEnterEvent const& event) override final
		{
			Json::Value message;
			message["text"] = event.player->name().toStdString() + " joined.";
			message["color"] = "gold";
			broadcastMessage(world, message, ChatType::CHAT_MESSAGE);
		}

		virtual void handle(World& world, PlayerWorldLeaveEvent const& event) override final
		{
			Json::Value message;
			message["text"] = event.player->name().toStdString() + " left.";
			message["color"] = "gold";
			broadcastMessage(world, message, ChatType::CHAT_MESSAGE);
		}

		virtual void handle(World& world, NetworkPacketEvent const& event) override final
		{
			if(event.packet.header.type == clientPayloadIdOf<PacketClientChat>())
			{
				PacketClientChat chat;

				if(!decodePayload(event.packet, chat))
				{
					world.server().disconnectWithMessage(event.player->cid(), "invalid packet ClientChat");
					return;
				}

				if(chat.message.empty() || chat.message[0] == '/')
					return;

				auto playerName = event.player->name().toStdString();
				info("[%s] <%s> %s", world.name(), playerName, chat.message);

				Json::Value message;
				message["text"] = '<' + playerName + "> " + chat.message;

				broadcastMessage(world, message, ChatType::CHAT_MESSAGE);
			}
		}
	};

	void broadcastMessage(World& world, Json::Value const& message, ChatType type)
	{
		Json::FastWriter writer;

		PacketServerChat chat;
		chat.json = writer.write(message);
		chat.type = type;

		for(auto client : world.clients())
			world.netHandler().queuePacket(client, chat);
	}
}

QS_REGISTER_HANDLER(qs::ChatHandler)
