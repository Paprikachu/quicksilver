// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "GameTypes.hpp"

namespace qs
{
	struct EntityData
	{
		GlobalEntityId geid;
		WorldEntityId weid;

		EntityCoord coord;
		EntityLook look;
		bool onGround;
	};

	struct Entity
	{
		GlobalEntityId geid() const { return geid_; }
		WorldEntityId weid() const { return weid_; }

		EntityCoord coord() const { return coord_; }
		void coord(EntityCoord coord) { coord_ = coord; }
		EntityLook look() const { return look_; }
		void look(EntityLook look) { look_ = look; }
		bool onGround() const { return onGround_; }
		void onGround(bool onGround) { onGround_ = onGround; }

	protected:
		Entity(Entity const&) = default;
		Entity& operator = (Entity const&) = default;
		~Entity() = default;

		Entity(EntityData const& data)
			: geid_(data.geid), weid_(data.weid)
			, coord_(data.coord), look_(data.look), onGround_(data.onGround)
		{}

	private:
		GlobalEntityId geid_;
		WorldEntityId weid_;

		EntityCoord coord_;
		EntityLook look_;
		bool onGround_;
	};
}
