// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <boost/operators.hpp>

#include "qsutils/ArraySet.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct Entity;

	template <typename ConcreteEntity>
	struct EntityPtr;

	struct EntityBasePtr
		: boost::equality_comparable<EntityBasePtr>
		, boost::less_than_comparable<EntityBasePtr>
	{
		EntityBasePtr(EntityBasePtr const&) = default;
		EntityBasePtr& operator = (EntityBasePtr const&) = default;
		~EntityBasePtr() = default;

		EntityBasePtr()
			: cont_(nullptr)
			, index_(0)
			, get_(nullptr)
			, erase_(nullptr)
		{}

		template <typename T>
		EntityBasePtr(ArraySet<T>& cont, SizeT index)
			: cont_(&cont)
			, index_(index)
			, get_(&EntityBasePtr::doGet<T>)
			, erase_(EntityBasePtr::doErase<T>)
		{}

		Entity& operator * () const
		{
			return *get_(cont_, index_);
		}

		Entity* operator -> () const
		{
			return &**this;
		}

		void erase() const { erase_(cont_, index_); }

		template <typename ConcreteEntity>
		EntityPtr<ConcreteEntity> downcast() const;

	private:
		template <typename T>
		static Entity* doGet(void* cont, SizeT index)
		{
			return cont ? &(*static_cast<ArraySet<T>*>(cont))[index] : nullptr;
		}

		template <typename T>
		static void doErase(void* cont, SizeT index)
		{
			static_cast<ArraySet<T>*>(cont)->erase(index);
		}

		friend bool operator == (EntityBasePtr const& lhs, EntityBasePtr const& rhs)
		{
			return lhs.cont_ == rhs.cont_ && lhs.index_ == rhs.index_;
		}

		friend bool operator < (EntityBasePtr const& lhs, EntityBasePtr const& rhs)
		{
			if(lhs.cont_ == rhs.cont_)
				return lhs.index_ < rhs.index_;

			return lhs.cont_ < rhs.cont_;
		}

		void* cont_;
		SizeT index_;
		Entity* (*get_) (void*, SizeT);
		void (*erase_) (void*, SizeT);
	};

	template <typename ConcreteEntity>
	struct EntityPtr
		: boost::equality_comparable<EntityPtr<ConcreteEntity>>
		, boost::less_than_comparable<EntityPtr<ConcreteEntity>>
	{
		EntityPtr(EntityPtr const&) = default;
		EntityPtr& operator = (EntityPtr const&) = default;
		~EntityPtr() = default;

		EntityPtr()
			: cont_(nullptr), index_(0)
		{}

		EntityPtr(ArraySet<ConcreteEntity>& cont, SizeT index)
			: cont_(&cont), index_(index)
		{}

		ConcreteEntity& operator * () const
		{
			return (*cont_)[index_];
		}

		ConcreteEntity* operator -> () const
		{
			return &**this;
		}

		explicit operator bool() const
		{
			return operator -> ();
		}

	private:
		friend bool operator == (EntityPtr const& lhs, EntityPtr const& rhs)
		{
			return lhs.cont_ == rhs.cont_ && lhs.index_ == rhs.index_;
		}

		friend bool operator < (EntityPtr const& lhs, EntityPtr const& rhs)
		{
			if(lhs.cont_ == rhs.cont_)
				return lhs.index_ < rhs.index_;

			return lhs.cont_ < rhs.cont_;
		}

		ArraySet<ConcreteEntity>* cont_;
		SizeT index_;
	};

	template <typename ConcreteEntity>
	EntityPtr<ConcreteEntity> EntityBasePtr::downcast() const
	{
		return EntityPtr<ConcreteEntity>(*static_cast<ArraySet<ConcreteEntity>*>(cont_), index_);
	}
}