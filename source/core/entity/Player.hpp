// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>

#include "GameTypes.hpp"
#include "Inventory.hpp"
#include "entity/Entity.hpp"

namespace qs
{
	struct PlayerData
	{
		ClientId cid;
		Uuid uuid;
		PlayerName name;
	};

	struct Player : Entity
	{
		Player(EntityData const& entityData, PlayerData const& playerData)
			: Entity(entityData)
			, cold_(std::make_unique<ColdData>(playerData.uuid, playerData.name))
			, cid_(playerData.cid)
		{}

		ClientId cid() const { return cid_; }
		Uuid uuid() const { return cold_->uuid; }
		PlayerName name() const { return cold_->name; }

		Inventory& inventory() { return cold_->inventory; }
		Inventory const& inventory() const { return cold_->inventory; }

	private:
		struct ColdData
		{
			ColdData(Uuid uuid, PlayerName name)
				: uuid(uuid), name(name)
			{}

			Uuid uuid;
			PlayerName name;
			Inventory inventory;
		};

		std::unique_ptr<ColdData> cold_;
		ClientId cid_;
	};
}
