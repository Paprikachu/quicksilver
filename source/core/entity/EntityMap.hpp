// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstddef>
#include <typeinfo>
#include <unordered_map>
#include <utility>
#include <vector>

#include "entity/Entity.hpp"
#include "entity/EntityPtr.hpp"
#include "entity/Player.hpp"
#include "qsutils/ArrayMap.hpp"
#include "qsutils/ArraySet.hpp"
#include "qsutils/IdGenerator.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct EntityMap
	{
		template <typename Entity, typename... Args>
		EntityPtr<Entity> createEntity(EntityData const& entityData, Args&&... args)
		{
			auto& set = setOf<Entity>();
			auto index = set.emplace(entityData, std::forward<Args>(args)...);
			entitiesByWeid_.emplace(entityData.weid, set, index);
			entitiesByGeid_.emplace(entityData.geid, set, index);
			return EntityPtr<Entity>(set, index);
		}

		void destroyEntity(WorldEntityId weid)
		{
			auto value = entitiesByWeid_.find(weid);
			QS_ASSERT(value, "attempted to destroy already-destroyed WorldEntityId")
			auto geid = (*value)->geid();
			value->erase();
			entitiesByWeid_.erase(weid);
			entitiesByGeid_.erase(geid);
		}

		template <typename Entity, typename F>
		void iterate(F&& f)
		{
			setOf<Entity>().iterate(std::forward<F>(f));
		}

		EntityBasePtr find(WorldEntityId weid)
		{
			auto entity = entitiesByWeid_.find(weid);

			if(!entity)
				return {};

			return *entity;
		}

		EntityBasePtr find(GlobalEntityId geid)
		{
			auto entity = entitiesByGeid_.find(geid);

			if(!entity)
				return {};

			return *entity;
		}

	private:

		// TODO: replace this stuff by compiletime mechanism
#define QS_ENTITYMAP_MEMBER(name, ...) { &typeid(__VA_ARGS__), offsetof(EntityMap, name) }

		template <typename Entity>
		ArraySet<Entity>& setOf()
		{
			static std::unordered_map<std::type_info const*, SizeT> members =
			{
				QS_ENTITYMAP_MEMBER(players_, Player)
			};

			auto it = members.find(&typeid(Entity));
			QS_ASSERT(it != members.end(), "invalid type requested")
			auto offset = it->second;
			return *reinterpret_cast<ArraySet<Entity>*>(reinterpret_cast<char*>(this) + offset);
		}

#undef QS_ENTITYMAP_MEMBER

		ArrayMap<WorldEntityId, EntityBasePtr> entitiesByWeid_;
		ArrayMap<GlobalEntityId, EntityBasePtr> entitiesByGeid_;
		ArraySet<Player> players_;
	};
}
