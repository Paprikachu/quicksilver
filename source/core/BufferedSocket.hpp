// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <utility>

#include <boost/asio/buffer.hpp>
#include <boost/asio/completion_condition.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/ip/tcp.hpp>

#include "qsutils/NamespaceShorthands.hpp"

namespace qs
{
	struct StreamBufferedSocket
	{
		StreamBufferedSocket(btcp::socket socket)
			: socket_(std::move(socket))
		{}

		template <typename... Args>
		void asyncReadUntil(Args&&... args)
		{
			ba::async_read_until(socket_, buffer_, std::forward<Args>(args)...);
		}

		template <typename Buffer, typename Handler>
		void asyncWrite(Buffer&& buffer, Handler&& handler)
		{
			ba::async_write(socket_, std::forward<Buffer>(buffer), ba::transfer_all(), std::forward<Handler>(handler));
		}

		btcp::endpoint remoteEndpoint() const { return socket_.remote_endpoint(); }
		std::streambuf& buffer() { return buffer_; }

	private:
		btcp::socket socket_;
		ba::streambuf buffer_;
	};
}
