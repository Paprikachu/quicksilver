// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cstring>
#include <stdexcept>
#include <utility>

#include "ChunkGenerator.hpp"
#include "World.hpp"
#include "handlers/ChunkHandler.hpp"

namespace qs
{
	namespace
	{
		qsapi::ChunkData findOrGenerateChunk(void* context, qsapi::ChunkCoord coord)
		{
			auto& world = *static_cast<World*>(context);
			auto& chunk = findOrGenerateChunkAt(world, { coord.x, coord.z });

			qsapi::ChunkData data;
			data.blocks = chunk.blocks();
			data.biomes = chunk.biomes();
			return data;
		}

		qsapi::ChunkCoord coordForChunk(Chunk& chunk)
		{
			qsapi::ChunkCoord coord;
			coord.x = chunk.coord().x();
			coord.z = chunk.coord().z();
			return coord;
		}

		qsapi::ChunkData dataForChunk(Chunk& chunk)
		{
			qsapi::ChunkData data;
			data.blocks = chunk.blocks();
			data.biomes = chunk.biomes();
			return data;
		}
	}

	ChunkGenerator::ChunkGenerator(World& world, std::string const& name, UInt64 seed, std::string const& parameters)
		: lib_(Library::open("generators/" + name))
		, generate_(reinterpret_cast<void (*) (void*, qsapi::ChunkCoord, qsapi::ChunkData)>(lib_.find("generate")))
		, populate_(reinterpret_cast<void (*) (void*, qsapi::ChunkCoord, qsapi::ChunkData)>(lib_.find("populate")))
		, destroy_(reinterpret_cast<void (*) (void*)>(lib_.find("destroy")))
		, instance_(nullptr, nullptr)
	{
		auto create = reinterpret_cast<void* (*) (UInt64, char const*, qsapi::GeneratorData)>(lib_.find("create"));

		if(!create || !generate_ || !populate_ || !destroy_)
			throw std::runtime_error("error loading generator - did you forget to register the generator type?");

		qsapi::GeneratorData data;
		data.context = &world;
		data.findOrGenerateChunk = findOrGenerateChunk;
		instance_ = decltype(instance_)(create(seed, parameters.c_str(), data), destroy_ );
	}

	void ChunkGenerator::generate(Chunk& chunk)
	{
		generate_(instance_.get(), coordForChunk(chunk), dataForChunk(chunk));
	}

	void ChunkGenerator::populate(Chunk& chunk)
	{
		populate_(instance_.get(), coordForChunk(chunk), dataForChunk(chunk));

		for(UInt8 i = 0; i != 16; ++i)
		{
			auto begin = chunk.sectionBlockData(i);
			auto end = begin + CHUNK_SECTION_BLOCKS_SIZE;

			if(std::find_if(begin, end, [](UInt16 type){ return type != 0; }) != end)
				chunk.markSections(1 << i);
		}

		std::memset(chunk.skyLight(), 0xFF, sizeof *chunk.skyLight());
	}
}
