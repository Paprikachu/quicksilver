// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <atomic>
#include <chrono>
#include <functional>
#include <thread>

#include <boost/container/flat_set.hpp>

#include "GameTypes.hpp"
#include "WorldNetHandler.hpp"
#include "entity/EntityMap.hpp"
#include "event/Events.hpp"
#include "event/HandlerFactoryRegistry.hpp"
#include "event/HandlerRegistry.hpp"
#include "qsutils/ArrayMap.hpp"
#include "qsutils/IdGenerator.hpp"
#include "qsutils/JsonUtils.hpp"
#include "qsutils/NamespaceShorthands.hpp"
#include "qsutils/TsQueue.hpp"

namespace qs
{
	struct WorldSettings
	{
		Gamemode gamemode;
		Dimension dimension;
		Difficulty difficulty;
		LevelType levelType;
	};

	struct MinecraftServer;

	struct World
	{
		using Task = std::function<void(World&)>;

		World(World const&) = delete;
		World& operator = (World const&) = delete;
		World(World&&) = delete;
		World& operator = (World&&) = delete;

		World(MinecraftServer& server, WorldId id, std::string const& name)
			: World(server, id, name, loadJsonFromFile(format("worlds/%s/settings.json", name)))
		{}

		~World()
		{
			run_ = false;
			thread_.join();
		}

		WorldId id() const { return id_; }
		std::string const& name() const { return name_; }
		WorldSettings const& settings() const { return settings_; }
		EntityCoord spawnPoint() const { return spawnPoint_; }

		MinecraftServer& server() { return *server_; }

		template <typename F>
		void queueTask(F&& task)
		{
			tasks_.push(std::forward<F>(task));
		}

		GlobalEntityId translateId(WorldEntityId weid)
		{
			return entities_.find(weid)->geid();
		}

		WorldEntityId translateId(GlobalEntityId geid)
		{
			return entities_.find(geid)->weid();
		}

		WorldEntityId translateId(ClientId id)
		{
			return *clientIdToWorldId_.find(id);
		}

		bc::flat_set<ClientId> const& clients() const
		{
			return clients_;
		}

		void handlePlayerEnter(ClientId cid, Uuid uuid, GlobalEntityId geid, PlayerName name)
		{
			auto weid = weidGen_.createId();
			auto entityData = EntityData{geid, weid, spawnPoint_, {0, 0}, false};
			auto playerData = PlayerData{cid, uuid, name};

			auto player = entities_.createEntity<Player>(entityData, playerData);
			clientIdToWorldId_.emplace(cid, weid);
			clients_.insert(cid);

			PacketJoinGame join;
			join.entityId = geid;
			join.difficulty = settings_.difficulty;
			join.dimension = settings_.dimension;
			join.gamemode = settings_.gamemode;
			join.levelType = toString(settings_.levelType);
			join.maxPlayers = 255;
			join.reducedDebugInfo = false;
			netHandler_.queuePacket(cid, join);

			PacketSpawnPoint spawn;
			spawn.coord = toBlockCoord(spawnPoint_);
			netHandler_.queuePacket(cid, spawn);

			PacketPlayerAbilities abilities;
			abilities.flyingSpeed = 0.1f;
			abilities.walkingSpeed = 0.1f;
			abilities.flags = AbilityFlags::GODMODE | AbilityFlags::CANFLY | AbilityFlags::FLYING | AbilityFlags::CREATIVE;
			netHandler_.queuePacket(cid, abilities);

			PacketPlayerPositionLook posAndLook;
			posAndLook.coord = player->coord();
			posAndLook.look = player->look();
			posAndLook.flags = PositionLookFlags::NONE;
			netHandler_.queuePacket(cid, posAndLook);

			PlayerWorldEnterEvent event;
			event.player = player;
			dispatchEvent(event);
		}

		void handlePlayerLeave(ClientId cid)
		{
			auto weid = *clientIdToWorldId_.find(cid);
			clientIdToWorldId_.erase(cid);
			clients_.erase(cid);

			PlayerWorldLeaveEvent event;
			event.player = entities_.find(weid).downcast<Player>();
			dispatchEvent(event);

			entities_.destroyEntity(weid);
			weidGen_.destroyId(weid);
		}

		template <typename Event>
		void dispatchEvent(Event& event)
		{
			handlers_.dispatch(*this, event);
		}

		EntityMap& entities() { return entities_; }
		HandlerRegistry& handlers() { return handlers_; }
		WorldNetHandler& netHandler() { return netHandler_; }
		Json::Value const& config() const { return config_; }

	private:
		World(MinecraftServer& server, WorldId id, std::string const& name, Json::Value const& config)
			: server_(&server)
			, id_(id), name_(name)
			, settings_(WorldSettings{ Gamemode::CREATIVE, Dimension::OVERWORLD, Difficulty::PEACEFUL, LevelType::DEFAULT })
			, spawnPoint_({ find<Float64>(config, "spawn.x"), find<Float64>(config, "spawn.y"), find<Float64>(config, "spawn.z") })
			, config_(config)
			, netHandler_(*this)
			, handlers_(createHandlers(*this))
			, run_(true)
			, thread_(&World::tickLoop, this)
		{}

		void tickLoop()
		{
			using namespace std::chrono_literals;

			while(run_)
			{
				auto before = std::chrono::high_resolution_clock::now();

				netHandler_.preTick();

				WorldTickEvent event;
				dispatchEvent(event);

				for(auto& task : tasks_.takeAll())
					task(*this);

				netHandler_.postTick();

				auto after = std::chrono::high_resolution_clock::now();

				// system time changed back
				if(after < before)
					continue;

				auto diff = after - before;
				
				// 20 tps (minecraft default)
				if(diff < 50ms)
					std::this_thread::sleep_for(50ms - diff);
			}
		}

		MinecraftServer* server_;

		WorldId id_;
		std::string name_;
		WorldSettings settings_;
		EntityCoord spawnPoint_;
		Json::Value config_;

		IdGenerator<WorldEntityId> weidGen_;
		EntityMap entities_;

		ArrayMap<ClientId, WorldEntityId> clientIdToWorldId_;
		bc::flat_set<ClientId> clients_;

		TsQueue<Task> tasks_;
		WorldNetHandler netHandler_;

		// this must be as far down as reasonably possible, because handlers may use the world during construction
		HandlerRegistry handlers_;

		std::atomic_bool run_;
		std::thread thread_;
	};
}
