// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <memory>

#include "MinecraftServer.hpp"
#include "World.hpp"
#include "WorldNetHandler.hpp"

namespace qs
{
	static_assert(WorldNetHandler::BUFFER_SIZE >= PACKET_MAX_SIZE, "buffer size too small to hold biggest packet");
	static_assert(WorldNetHandler::HUGE_BUFFER_SIZE >= PACKET_MAX_SIZE, "buffer size too small to hold biggest packet");

	WorldNetHandler::WorldNetHandler(World& world)
		: world_(&world)
		, bmgr_(BUFFER_SIZE), hbmgr_(HUGE_BUFFER_SIZE)
	{}

	void WorldNetHandler::preTick()
	{
		world_->server().popPackets(world_->clients(),
			[this](std::queue<std::tuple<ClientId, Packet>> packets)
			{
				auto sp = std::make_shared<std::queue<std::tuple<ClientId, Packet>>>(std::move(packets));

				world_->queueTask(
					[this, sp](World&)
					{
						while(!sp->empty())
						{
							auto& kv = sp->front();
							auto weid = world_->translateId(std::get<0>(kv));
							handlePacket(world_->entities().find(weid).downcast<Player>(), std::get<1>(kv));
							sp->pop();
						}
					});
			});
	}

	void WorldNetHandler::postTick()
	{
		if(!outgoingPackets_.empty())
		{
			world_->server().pushPackets(std::move(outgoingPackets_));
			outgoingPackets_ = std::queue<std::tuple<ClientId, BufferManager::UniqueRef, SizeT>>();
		}
	}

	void WorldNetHandler::handlePacket(EntityPtr<Player> player, Packet& packet)
	{
		NetworkPacketEvent event;
		event.player = player;
		event.packet = std::move(packet);
		world_->dispatchEvent(event);
	}
}
