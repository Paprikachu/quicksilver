// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <string>

#include "api/GeneratorApi.hpp"
#include "qsutils/Library.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct Chunk;
	struct World;

	struct ChunkGenerator
	{
		ChunkGenerator(World& world, std::string const& name, UInt64 seed, std::string const& parameters);

		void generate(Chunk& chunk);
		void populate(Chunk& chunk);

	private:
		Library lib_;
		void (* generate_) (void* instance, qsapi::ChunkCoord coord, qsapi::ChunkData data) = nullptr;
		void (* populate_) (void* instance, qsapi::ChunkCoord coord, qsapi::ChunkData data) = nullptr;
		void (* destroy_) (void* instance) = nullptr;
		std::unique_ptr<void, void (*) (void*)> instance_;
	};
}
