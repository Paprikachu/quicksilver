// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memory>
#include <string>

#include <boost/container/flat_map.hpp>

#include <json/json.h>

#include "GameTypes.hpp"
#include "World.hpp"
#include "qsutils/ArrayMap.hpp"
#include "qsutils/IdGenerator.hpp"
#include "qsutils/NamespaceShorthands.hpp"

namespace qs
{
	struct MinecraftServer;

	struct WorldManager
	{
		WorldManager(MinecraftServer& server, Json::Value const& config);

		WorldId loadWorld(std::string const& name);
		World& mainWorld();

	private:
		MinecraftServer* server_;

		IdGenerator<WorldId> idGenerator_;
		ArrayMap<WorldId, std::unique_ptr<World>> worlds_;
		bc::flat_map<std::string, World*> worldsByName_;
		World* mainWorld_;
	};
}
