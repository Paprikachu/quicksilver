// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cstring>
#include <stdexcept>
#include <type_traits>
#include <utility>

#include "GameTypes.hpp"
#include "protocol/Decoder.hpp"

namespace qs
{
	namespace
	{
		bool nbtRead(UInt8 const*& buffer, SizeT& size, void* dest, SizeT bytes)
		{
			if(size < bytes)
				return false;

			std::memcpy(dest, buffer, bytes);
			buffer += bytes;
			size -= bytes;
			return true;
		}

		template <typename T, std::enable_if_t<std::is_integral<T>::value>* = nullptr>
		bool nbtRead(UInt8 const*& buffer, SizeT& size, T& value)
		{
			return decode(buffer, size, value);
		}

		template <typename T, std::enable_if_t<std::is_floating_point<T>::value>* = nullptr>
		bool nbtRead(UInt8 const*& buffer, SizeT& size, T& value)
		{
			return decode(buffer, size, value);
		}

		bool nbtRead(UInt8 const*& buffer, SizeT& size, std::string& out)
		{
			UInt16 length;

			if(!decode(buffer, size, length))
				return false;

			out.resize(length);
			return nbtRead(buffer, size, const_cast<char*>(out.data()), out.size());
		}

		template <typename T>
		bool nbtRead(UInt8 const*& buffer, SizeT& size, std::vector<T>& out)
		{
			Int32 length;

			if(!nbtRead(buffer, size, length) || length < 0)
				return false;

			out.resize(static_cast<UInt32>(length));

			for(auto& elem : out)
				if(!nbtRead(buffer, size, elem))
					return false;

			return true;
		}

		template <typename T>
		bool nbtReadValue(UInt8 const*& buffer, SizeT& size, NbtValue& value)
		{
			T result;

			if(!nbtRead(buffer, size, result))
				return false;

			value = std::move(result);
			return true;
		}

		bool nbtReadValue(UInt8 const*& buffer, SizeT& size, NbtValue& value, Int8 type)
		{
			switch(type)
			{
			case 1: return nbtReadValue<Int8>(buffer, size, value);
			case 2: return nbtReadValue<Int16>(buffer, size, value);
			case 3: return nbtReadValue<Int32>(buffer, size, value);
			case 4: return nbtReadValue<Int64>(buffer, size, value);
			case 5: return nbtReadValue<Float32>(buffer, size, value);
			case 6: return nbtReadValue<Float64>(buffer, size, value);
			case 7: return nbtReadValue<std::vector<Int8>>(buffer, size, value);
			case 8: return nbtReadValue<std::string>(buffer, size, value);
			case 11: return nbtReadValue<std::vector<Int32>>(buffer, size, value);

			case 9:
				{
					Int8 elementType;

					if(!nbtRead(buffer, size, elementType))
						return false;

					Int32 length;

					if(!nbtRead(buffer, size, length) || length < 0)
						return false;

					std::vector<NbtValue> result;
					result.resize(static_cast<UInt32>(length));

					for(auto& elem : result)
						if(!nbtReadValue(buffer, size, elem, elementType))
							return false;

					value = std::move(result);
					return true;
				}

			case 10:
				{
					bc::flat_set<Nbt> result;

					for(;;)
					{
						if(size == 0)
							return false;

						if(*buffer == 0)
							break;

						Nbt nbt;
						
						if(!decode(buffer, size, nbt))
							return false;

						result.insert(std::move(nbt));
					}

					value = std::move(result);
					return true;
				}

			default: break;
			}

			return false;
		}
	}

	bool decode(UInt8 const*& buffer, SizeT& size, Nbt& nbt)
	{
		Int8 type;

		if(!nbtRead(buffer, size, type))
			return false;

		if(!nbtRead(buffer, size, nbt.name))
			return false;

		return nbtReadValue(buffer, size, nbt.value, type);
	}

	void encode(UInt8*& buffer, Nbt const& nbt)
	{
		(void)buffer;
		(void)nbt;
		throw std::runtime_error("nbt encoding nyi");
	}

	SizeT wireLength(Nbt const& nbt)
	{
		(void)nbt;
		throw std::runtime_error("nbt encoding nyi");
	}
}