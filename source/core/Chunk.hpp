// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstring>

#include <boost/container/flat_set.hpp>

#include "GameTypes.hpp"
#include "entity/EntityPtr.hpp"
#include "entity/Player.hpp"
#include "qsutils/Assertions.hpp"
#include "qsutils/NamespaceShorthands.hpp"

namespace qs
{
	struct World;

	struct Chunk
	{
		Chunk() = delete;
		Chunk(Chunk const&) = delete;
		Chunk& operator = (Chunk const&) = delete;
		Chunk(Chunk&&) = delete;
		Chunk& operator = (Chunk&&) = delete;
		~Chunk() = default;

		Chunk(ChunkCoord pos)
			: pos_(pos)
		{
			std::memset(biomes_, -1, sizeof biomes_);
		}

		ChunkCoord coord() const { return pos_; }
		UInt16 sectionBitmap() const { return sectionBitmap_; }
		bool populated() { return populated_; }

		UInt16& blockAt(ChunkBlockCoord coord)
		{
			return blocks_[coord.y()][coord.z()][coord.x()];
		}

		void markPopulated() { populated_ = true; }
		void markSections(UInt16 sections) { sectionBitmap_ |= sections; }

		UInt16 const* blockData() const { return &blocks_[0][0][0]; }
		UInt8 const* blockLightData() const { return &blockLight_[0][0][0]; }
		UInt8 const* skyLightData() const { return &skyLight_[0][0][0]; }

		UInt16 const* sectionBlockData(SizeT section) const { return &blocks_[section * 16][0][0]; }
		UInt8 const* sectionBlockLightData(SizeT section) const { return &blockLight_[section * 16][0][0]; }
		UInt8 const* sectionSkyLightData(SizeT section) const { return &skyLight_[section * 16][0][0]; }

		UInt8 const* biomeData() const { return &biomes_[0][0]; }

		UInt16 (* blocks()) [256][16][16]
		{
			return &blocks_;
		}

		UInt8 (* biomes()) [16][16]
		{
			return &biomes_;
		}

		UInt8 (* blockLight()) [256][16][8]
		{
			return &blockLight_;
		}

		UInt8 (* skyLight()) [256][16][8]
		{
			return &skyLight_;
		}

		void subscribe(EntityPtr<Player> player)
		{
			auto inserted = subscribers_.insert(player).second;
			QS_ASSERT(inserted, "subscribe %s: duplicate subscription for player %s", pos_, player->name())
		}

		void unsubscribe(EntityPtr<Player> player)
		{
			auto erased = subscribers_.erase(player);
			QS_ASSERT(erased == 1, "unsubscribe %s: player '%s' has no subscription", pos_, player->name())
		}

		bc::flat_set<EntityPtr<Player>> const& subscribers() const
		{
			return subscribers_;
		}

		void enter(EntityPtr<Player> player)
		{
			auto inserted = members_.insert(player).second;
			QS_ASSERT(inserted, "enter %s: player % already member", pos_, player->name())
		}

		void leave(EntityPtr<Player> player)
		{
			auto erased = members_.erase(player);
			QS_ASSERT(erased == 1, "leave %s: player %s not in chunk", pos_, player->name())
		}

		bc::flat_set<EntityPtr<Player>> const& members() const
		{
			return members_;
		}

	private:
		bc::flat_set<EntityPtr<Player>> subscribers_;
		bc::flat_set<EntityPtr<Player>> members_;
		ChunkCoord pos_;
		UInt16 sectionBitmap_ = 0;
		UInt16 blocks_[256][16][16] = {};
		UInt8 blockLight_[256][16][8] = {};
		UInt8 skyLight_[256][16][8] = {};
		UInt8 biomes_[16][16];
		bool populated_ = false;
	};

	constexpr SizeT CHUNK_SECTION_BLOCKS_SIZE = 16 * 16 * 16 * 2;
	constexpr SizeT CHUNK_SECTION_BLOCKLIGHT_SIZE = 8 * 16 * 16;
	constexpr SizeT CHUNK_SECTION_SKYLIGHT_SIZE = 8 * 16 * 16;
	constexpr SizeT CHUNK_SECTION_SIZE_NO_SKYLIGHT = CHUNK_SECTION_BLOCKS_SIZE + CHUNK_SECTION_BLOCKLIGHT_SIZE;
	constexpr SizeT CHUNK_BIOME_SIZE = 16 * 16;
}
