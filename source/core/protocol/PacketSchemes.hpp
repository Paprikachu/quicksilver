// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// #pragma once intentionally left out

#include "GameTypes.hpp"

#ifndef QS_PACKET_BEGIN
#define QS_PACKET_BEGIN(name, sid, cid)
#endif

#ifndef QS_PACKET_MEMBER
#define QS_PACKET_MEMBER(name, ...)
#endif

#ifndef QS_PACKET_END
#define QS_PACKET_END()
#endif

QS_PACKET_BEGIN(PacketHandshake, -1, 0x00)
QS_PACKET_MEMBER(version,  qs::VarInt<qs::UInt32>)
QS_PACKET_MEMBER(hostname, std::string)
QS_PACKET_MEMBER(port,     qs::UInt16)
QS_PACKET_MEMBER(next,     qs::VarInt<qs::UInt32>)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketStatusRequest, -1, 0x00)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketStatusResponse, 0x00, -1)
QS_PACKET_MEMBER(json, std::string)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketPing, 0x01, 0x01)
QS_PACKET_MEMBER(time, qs::Int64)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketLoginStart, -1, 0x00)
QS_PACKET_MEMBER(name, qs::PlayerName)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketLoginSuccess, 0x02, -1)
QS_PACKET_MEMBER(uuid, std::string)
QS_PACKET_MEMBER(name, qs::PlayerName)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketJoinGame, 0x01, -1)
QS_PACKET_MEMBER(entityId,         qs::GlobalEntityId)
QS_PACKET_MEMBER(gamemode,         qs::Gamemode)
QS_PACKET_MEMBER(dimension,        qs::Dimension)
QS_PACKET_MEMBER(difficulty,       qs::Difficulty)
QS_PACKET_MEMBER(maxPlayers,       qs::UInt8)
QS_PACKET_MEMBER(levelType,        std::string)
QS_PACKET_MEMBER(reducedDebugInfo, bool)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketSpawnPoint, 0x05, -1)
QS_PACKET_MEMBER(coord, qs::BlockCoord)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketPlayerAbilities, 0x39, -1)
QS_PACKET_MEMBER(flags,        qs::AbilityFlags)
QS_PACKET_MEMBER(flyingSpeed,  qs::Float32)
QS_PACKET_MEMBER(walkingSpeed, qs::Float32)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketKeepAlive, 0x00, 0x00)
QS_PACKET_MEMBER(id, qs::VarInt<qs::Int32>)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketDisconnect, 0x40, -1)
QS_PACKET_MEMBER(json, std::string)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketPlayerPositionLook, 0x08, 0x06)
QS_PACKET_MEMBER(coord, qs::EntityCoord)
QS_PACKET_MEMBER(look,  qs::EntityLook)
QS_PACKET_MEMBER(flags, qs::PositionLookFlags)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketPlayerPosition, -1, 0x04)
QS_PACKET_MEMBER(coord,    qs::EntityCoord)
QS_PACKET_MEMBER(onGround, bool)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketClientChat, -1, 0x01)
QS_PACKET_MEMBER(message, std::string)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketServerChat, 0x02, -1)
QS_PACKET_MEMBER(json, std::string)
QS_PACKET_MEMBER(type, qs::ChatType)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketSetCompression, 0x03, -1)
QS_PACKET_MEMBER(threshold, qs::VarInt<qs::Int32>)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketPlayerDigging, -1, 0x07)
QS_PACKET_MEMBER(status, qs::DigStatus)
QS_PACKET_MEMBER(location, qs::BlockCoord)
QS_PACKET_MEMBER(face, qs::BlockFace)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketBlockChange, 0x23, -1)
QS_PACKET_MEMBER(location, qs::BlockCoord)
QS_PACKET_MEMBER(type, qs::VarInt<qs::Block>)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketPlayerBlockPlacement, -1, 0x08)
QS_PACKET_MEMBER(location, qs::BlockCoord)
QS_PACKET_MEMBER(face, qs::BlockFace)
QS_PACKET_MEMBER(slot, qs::SlotData)
QS_PACKET_MEMBER(cursorX, qs::Int8)
QS_PACKET_MEMBER(cursorY, qs::UInt8)
QS_PACKET_MEMBER(cursorZ, qs::Int8)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketCreativeInventoryAction, -1, 0x10)
QS_PACKET_MEMBER(slot, qs::Int16)
QS_PACKET_MEMBER(item, qs::SlotData)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketHeldItemChange, -1, 0x09)
QS_PACKET_MEMBER(slot, qs::Int16)
QS_PACKET_END()

QS_PACKET_BEGIN(PacketSpawnPlayer, 0x0C, -1)
QS_PACKET_MEMBER(geid, qs::VarInt<qs::GlobalEntityId>)
QS_PACKET_MEMBER(uuid, qs::Uuid)
QS_PACKET_MEMBER(coord, qs::FixedEntityCoord)
QS_PACKET_MEMBER(look, qs::EntityLook)
QS_PACKET_MEMBER(item, qs::Int16)
QS_PACKET_MEMBER(metadata, qs::Metadata)
QS_PACKET_END()