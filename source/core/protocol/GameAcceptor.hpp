// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstring>
#include <functional>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <string>
#include <tuple>
#include <utility>

#include <boost/asio/buffer.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/system/error_code.hpp>

#include <json/json.h>

#include "GameTypes.hpp"
#include "protocol/Encoder.hpp"
#include "protocol/Packets.hpp"
#include "qsutils/ArrayMap.hpp"
#include "qsutils/Buffer.hpp"
#include "qsutils/BufferManager.hpp"
#include "qsutils/IdGenerator.hpp"
#include "qsutils/Logging.hpp"
#include "qsutils/NamespaceShorthands.hpp"
#include "qsutils/Unreachable.hpp"

namespace qs
{
	using namespace std::placeholders;

	enum struct ClientState
	{
		INIT,
		PING,
		PLAY,
	};

	struct MinecraftServer;
	struct WorldManager;

	struct GameAcceptor
	{
		GameAcceptor(ba::io_service& service, Json::Value const& config, WorldManager& wmgr, MinecraftServer& server);

		template <typename F>
		void popPackets(bc::flat_set<ClientId> ids, F&& f)
		{
			std::queue<std::tuple<ClientId, Packet>> packets;

			for(auto id : ids)
			{
				if(auto value = clients_.find(id))
				{
					auto& q = (*value)->recvPackets;

					while(!q.empty())
					{
						packets.push(std::make_tuple(id, std::move(q.front())));
						q.pop();
					}
				}
			}

			f(std::move(packets));
		}

		void pushPackets(std::queue<std::tuple<ClientId, BufferManager::UniqueRef, SizeT>> packets)
		{
			while(!packets.empty())
			{
				auto id = std::get<0>(packets.front());

				if(auto client = clients_.find(id))
				{
					(*client)->sendBuffers.push(SizedBuffer(std::move(std::get<1>(packets.front())), std::get<2>(packets.front())));
					startAsyncWrite(**client);
				}

				packets.pop();
			}
		}

		void disconnect(ClientId id);
		void disconnectWithMessage(ClientId id, std::string const& message);

	private:
		using BufferRef = BufferManager::UniqueRef;

		struct ClientData;

		struct SizedBuffer
		{
			SizedBuffer() : buffer(), size(), handler() {}

			SizedBuffer(BufferRef&& buffer, SizeT size)
				: buffer(std::move(buffer)), size(size), handler()
			{}

			SizedBuffer(BufferRef&& buffer, SizeT size, std::function<void(ClientData&)>&& handler)
				: buffer(std::move(buffer)), size(size), handler(std::move(handler))
			{}

			BufferRef buffer;
			SizeT size;
			std::function<void(ClientData&)> handler;
		};

		struct ClientData
		{
			ClientData(BufferRef&& recvBuffer, btcp::socket socket)
				: recvBuffer(std::move(recvBuffer))
				, socket(std::move(socket))
				, uuid()
			{}

			std::queue<Packet> recvPackets;

			BufferRef recvBuffer;
			SizeT recvSize = 0;

			std::queue<SizedBuffer> sendBuffers;
			SizedBuffer current;

			ClientState state = ClientState::INIT;
			bool compression = false;
			btcp::socket socket;

			ClientId id{0};
			Uuid uuid;
			GlobalEntityId geid{0};
			PlayerName name;
		};

		void startAccept();
		void acceptHandler(bs::error_code ec);

		void startPing(ClientData& client);
		void startLogin(ClientData& client);
		void startReadLoop(ClientData& client);
		void startHandshake(ClientData& client);

		void disconnect(ClientData& client);

		template <typename PacketHandler>
		void asyncReadPacket(ClientData& client, PacketHandler handler)
		{
			PacketHeader header;
			UInt8 const* offptr = client.recvBuffer.data();

			SizeT bytesReceived = client.recvSize;
			auto result = tryDecodePacket(offptr, bytesReceived, client.compression, header);

			switch(result)
			{
			case PacketDecodeResult::NEED_MORE_BYTES:
				startAsyncRead(client, std::bind(
					[&client, handler, this](bs::error_code ec, SizeT size)
					{
						if(ec)
						{
							if(ec != ba::error::operation_aborted)
								disconnect(client);

							return;
						}

						client.recvSize += size;
						asyncReadPacket(client, handler);
					}, _1, _2));
				break;

			case PacketDecodeResult::INVALID_LENGTH:
				info("status: invalid length: length %s, type %s, bytes in buffer: %s", header.length, header.type, client.recvSize);
				disconnect(client);
				break;

			case PacketDecodeResult::SUCCESS:
				{
					auto recvBuffer = buffers_.aquire();
					std::memcpy(recvBuffer.data(), offptr, bytesReceived);
					auto packetBuffer = std::exchange(client.recvBuffer, std::move(recvBuffer));

					client.recvSize = bytesReceived;

					Packet packet;
					packet.header = header;
					packet.buffer = std::move(packetBuffer);

					handler(client, std::move(packet));
				}
				break;

			default:
				QS_UNREACHABLE("unhandled enum value")
				break;
			}
		}

		template <typename Handler>
		void startAsyncRead(ClientData& client, Handler&& handler)
		{
			client.socket.async_read_some(ba::buffer(client.recvBuffer.data() + client.recvSize, PACKET_MAX_SIZE - client.recvSize), std::forward<Handler>(handler));
		}

		void startAsyncWrite(ClientData& client);

		template <typename Packet>
		void asyncWritePacket(ClientData& client, Packet const& packet, std::function<void(ClientData&)> handler)
		{
			auto buffer = buffers_.aquire();
			auto length = encodePacket(buffer.data(), PACKET_MAX_SIZE, client.compression, packet);
			client.sendBuffers.push(SizedBuffer(std::move(buffer), length, std::move(handler)));
			startAsyncWrite(client);
		}

		template <typename Packet>
		void asyncWritePacket(ClientData& client, Packet const& packet)
		{
			asyncWritePacket(client, packet, {});
		}

		void updateStatusResponsePacket();

		void notifyJoin(ClientData& client);
		void notifyLeave(ClientData& client);

		void startKeepAliveTimer();

		std::string statusMessage_;
		UInt32 slotsMax_, slotsUsed_;
		PacketStatusResponse statusResponse_;

		boost::uuids::name_generator uuidGen_;

		btcp::acceptor acceptor_;
		btcp::socket socket_;

		boost::asio::deadline_timer keepAliveTimer_;

		BufferManager buffers_;

		IdGenerator<ClientId> clientIdGenerator_;
		ArrayMap<ClientId, std::unique_ptr<ClientData>> clients_;

		WorldManager* wmgr_;
		MinecraftServer* server_;
	};
}
