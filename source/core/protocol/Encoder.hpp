// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <algorithm>
#include <cstring>
#include <type_traits>

#include <boost/endian/conversion.hpp>

#include "GameTypes.hpp"
#include "protocol/VarInt.hpp"
#include "qsutils/FixedString.hpp"
#include "qsutils/StrongTypedefs.hpp"
#include "qsutils/Types.hpp"
#include "qsutils/Unreachable.hpp"

namespace qs
{
	namespace detail
	{
		inline void encodeVarIntImpl(UInt8*& buffer, UInt8 const* bytes, SizeT count)
		{
			while(--count)
				*buffer++ = *bytes++ | 0x80;

			*buffer++ = *bytes;
		}

		template <typename T>
		void encode(UInt8*& buffer, VarInt<T> value)
		{
			static constexpr SizeT ITERATION_LIMIT = (sizeof value * 8 + 6) / 7;

			UInt8 bytes[ITERATION_LIMIT];

			for(SizeT i = 0; i != ITERATION_LIMIT; ++i)
			{
				bytes[i] = value & 0x7F;
				value >>= 7;

				if(value == 0)
				{
					detail::encodeVarIntImpl(buffer, bytes, i + 1);
					return;
				}
			}

			QS_UNREACHABLE("iteration limit wrong?")
		}
	}

	template <typename T, T V>
	void encode(UInt8*& buffer, Constant<T, V>)
	{
		encode(buffer, V);
	}

	template <typename T>
	void encode(UInt8*& buffer, VarInt<T> value)
	{
		using UT = typename std::make_unsigned<T>::type;
		detail::encode(buffer, varInt(static_cast<UT>(value.get())));
	}

	template <typename T, typename DomainTag>
	void encode(UInt8*& buffer, VarInt<Integer<T, DomainTag>> value)
	{
		encode(buffer, varInt(value.get().value()));
	}

	template <typename Int, typename std::enable_if<std::is_integral<Int>::value>::type* = nullptr>
	void encode(UInt8*& buffer, Int value)
	{
		value = boost::endian::native_to_big(value);
		std::memcpy(buffer, &value, sizeof value);
		buffer += sizeof value;
	}

	template <typename Float, typename std::enable_if<std::is_floating_point<Float>::value>::type* = nullptr>
	void encode(UInt8*& buffer, Float value)
	{
		auto ptr = reinterpret_cast<char*>(&value);
		std::reverse(ptr, ptr + sizeof value);
		std::memcpy(buffer, &value, sizeof value);
		buffer += sizeof value;
	}

	template <typename Int, typename DomainTag>
	void encode(UInt8*& buffer, Integer<Int, DomainTag> value)
	{
		encode(buffer, value.value());
	}

	template <typename Length, Length Capacity>
	void encode(UInt8*& buffer, FixedString<char, Length, Capacity> const& s)
	{
		encode(buffer, varInt(s.length()));
		std::memcpy(buffer, s.data(), s.length());
		buffer += s.length();
	}

	inline void encode(UInt8*& buffer, UInt8 const* begin, UInt8 const* end)
	{
		QS_ASSERT(end >= begin, "[begin, end) is not a valid range")

		auto length = static_cast<SizeT>(end - begin);
		std::memcpy(buffer, begin, length);
		buffer += length;
	}

	template <typename Alloc, typename Traits>
	void encode(UInt8*& buffer, std::basic_string<char, Alloc, Traits> const& s)
	{
		encode(buffer, varInt(s.length()));
		auto data = reinterpret_cast<UInt8 const*>(s.data());
		encode(buffer, data, data + s.length());
	}

	inline void encode(UInt8*& buffer, Uuid uuid)
	{
		std::reverse(uuid.data, uuid.data + 16);
		encode(buffer, uuid.data, uuid.data + 16);
	}

	template <typename Enum, typename std::enable_if<std::is_enum<Enum>::value>::type* = nullptr>
	void encode(UInt8*& buffer, Enum value)
	{
		using UT = typename std::underlying_type<Enum>::type;
		encode(buffer, static_cast<UT>(value));
	}

	inline void encode(UInt8*& buffer, bool value)
	{
		encode(buffer, static_cast<UInt8>(value ? 1 : 0));
	}

	inline void encode(UInt8*& buffer, BlockCoord coord)
	{
		UInt64 value = ((coord.x() & 0x3FFFFFFull) << 38) | ((coord.y() & 0xFFFull) << 26) | (coord.z() & 0x3FFFFFFull);
		encode(buffer, value);
	}

	inline void encode(UInt8*& buffer, EntityCoord coord)
	{
		encode(buffer, coord.x());
		encode(buffer, coord.y());
		encode(buffer, coord.z());
	}

	inline void encode(UInt8*& buffer, EntityLook look)
	{
		encode(buffer, look.yaw());
		encode(buffer, look.pitch());
	}

	inline void encode(UInt8*& buffer, ChunkCoord coord)
	{
		encode(buffer, coord.x());
		encode(buffer, coord.z());
	}

	void encode(UInt8*& buffer, Nbt const& nbt);

	inline void encode(UInt8*& buffer, SlotData const& slot)
	{
		encode(buffer, slot.type);

		if(slot.type != -1)
		{
			encode(buffer, slot.count);
			encode(buffer, slot.damage);

			if(slot.data)
				encode(buffer, slot.data.get());
			else
				encode(buffer, Int8(0));
		}
	}

	inline void encode(UInt8*& buffer, FixedEntityCoord const& coord)
	{
		encode(buffer, coord.x());
		encode(buffer, coord.y());
		encode(buffer, coord.z());
	}

	void encode(UInt8*& buffer, Metadata const& data);
}
