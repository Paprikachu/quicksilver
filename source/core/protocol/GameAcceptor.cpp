// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>

#include <boost/asio/completion_condition.hpp>

#include "MinecraftServer.hpp"
#include "WorldManager.hpp"
#include "World.hpp"
#include "protocol/GameAcceptor.hpp"
#include "qsutils/JsonUtils.hpp"

namespace qs
{
	namespace
	{
		constexpr UInt32 PROTOCOL_VERSION = 47;
		constexpr char VERSION[] = "1.8";

		// https://gist.github.com/thinkofdeath/6927216
		// http://wiki.vg/Server_List_Ping#Ping_Process
		constexpr char PING_JSON[] = R"({"version":{"name":"%s","protocol":%s},"players":{"max":%s,"online":%s},"description":%s})";

		// http://wiki.vg/Chat
		constexpr char DISCONNECT_JSON[] = R"({"text":"%s","color":"red"})";

		Uuid const namespaceUuid = boost::uuids::string_generator()("01234567-89ab-cdef-0123-456789abcdef");
	}

	GameAcceptor::GameAcceptor(ba::io_service& service, Json::Value const& config, WorldManager& wmgr, MinecraftServer& server)
		: statusMessage_(Json::FastWriter().write(resolvePath(config, "description")))
		, slotsMax_(findInRange<UInt32>(config, "slots", 0, 10000))
		, slotsUsed_(0)
		, uuidGen_(namespaceUuid)
		, acceptor_(service, btcp::endpoint(
			findIp(config, "listen.address"),
			find<UInt16>(config, "listen.port")))
		, socket_(service)
		, keepAliveTimer_(service)
		, buffers_(PACKET_MAX_SIZE)
		, wmgr_(&wmgr)
		, server_(&server)
	{
		updateStatusResponsePacket();
		startAccept();
		startKeepAliveTimer();
	}

	void GameAcceptor::disconnect(ClientId id)
	{
		if(auto value = clients_.find(id))
			disconnect(**value);
	}

	void GameAcceptor::disconnectWithMessage(ClientId id, std::string const& message)
	{
		if(auto value = clients_.find(id))
		{
			auto& client = **value;

			PacketDisconnect dc;
			dc.json = format(DISCONNECT_JSON, message);

			asyncWritePacket(client, dc,
				[this](ClientData& client)
				{
					disconnect(client);
				});
		}
	}

	void GameAcceptor::startAccept()
	{
		acceptor_.async_accept(socket_, std::bind(&GameAcceptor::acceptHandler, this, _1));
	}

	void GameAcceptor::acceptHandler(bs::error_code ec)
	{
		if(ec)
		{
			if(ec == ba::error::operation_aborted)
				return;

			throw bs::system_error(ec);
		}

		auto id = clientIdGenerator_.createId();
		auto client = std::make_unique<ClientData>(buffers_.aquire(), std::move(socket_));
		client->id = id;
		auto pair = clients_.emplace(id, std::move(client));
		startHandshake(**pair.second);

		startAccept();
	}

	void GameAcceptor::startPing(ClientData& client)
	{
		asyncReadPacket(client,
			[this](ClientData& client, Packet packet)
			{
				PacketStatusRequest request;

				if(packet.header.type != clientPayloadIdOf<PacketStatusRequest>()
				|| !decodePayload(packet, request))
				{
					disconnect(client);
					return;
				}

				asyncWritePacket(client, statusResponse_,
					[this](ClientData& client)
					{
						asyncReadPacket(client,
							[this](ClientData& client, Packet packet)
							{
								PacketPing ping;

								if(packet.header.type != clientPayloadIdOf<PacketPing>()
								|| !decodePayload(packet, ping))
								{
									disconnect(client);
									return;
								}

								asyncWritePacket(client, ping,
									[this](ClientData& client)
									{
										disconnect(client);
									});
							});
					});
			});
	}

	void GameAcceptor::startLogin(ClientData& client)
	{
		asyncReadPacket(client,
			[this](ClientData& client, Packet packet)
			{
				PacketLoginStart login;

				if(packet.header.type != clientPayloadIdOf<PacketLoginStart>()
				|| !decodePayload(packet, login))
				{
					disconnect(client);
					return;
				}

				PacketSetCompression compression;
				compression.threshold = PACKET_MAX_SIZE + 1;

				asyncWritePacket(client, compression,
					[this, login](ClientData& client)
					{
						client.compression = true;

						PacketLoginSuccess success;

						auto uuid = uuidGen_(login.name.data(), login.name.length());
						success.uuid = boost::uuids::to_string(uuid);
						success.name = login.name;

						asyncWritePacket(client, success,
							[this, login, uuid](ClientData& client)
							{
								client.uuid = uuid;
								client.name = login.name;

								++slotsUsed_;
								updateStatusResponsePacket();

								notifyJoin(client);
								info("player '%s' connected", client.name);

								startReadLoop(client);
							});
					});
			});
	}

	void GameAcceptor::startReadLoop(ClientData& client)
	{
		asyncReadPacket(client,
			[this](ClientData& client, Packet packet)
			{
				if(packet.header.type != clientPayloadIdOf<PacketKeepAlive>())
					client.recvPackets.push(std::move(packet));

				startReadLoop(client);
			});
	}

	void GameAcceptor::startHandshake(ClientData& client)
	{
		asyncReadPacket(client,
			[this](ClientData& client, Packet packet)
			{
				PacketHandshake handshake;

				if(packet.header.type != clientPayloadIdOf<PacketHandshake>()
				|| !decodePayload(packet, handshake))
				{
					std::cout << "error decoding handshake" << std::endl;
					disconnect(client);
					return;
				}

				if(handshake.version != PROTOCOL_VERSION)
				{
					disconnect(client);
					return;
				}

				switch(handshake.next)
				{
				case 1: client.state = ClientState::PING; startPing(client); break;
				case 2: client.state = ClientState::PLAY; startLogin(client); break;
				default: disconnect(client); break;
				}
			});
	}

	void GameAcceptor::disconnect(ClientData& client)
	{
		clientIdGenerator_.destroyId(client.id);

		if(client.state == ClientState::PLAY)
		{
			info("player '%s' disconnected", client.name);
			notifyLeave(client);

			--slotsUsed_;
			updateStatusResponsePacket();
		}

		clients_.erase(client.id);
	}

	void GameAcceptor::notifyJoin(ClientData& client)
	{
		auto geid = server_->createGlobalEntityId();
		auto& world = wmgr_->mainWorld();

		auto id = client.id;
		auto uuid = client.uuid;
		auto name = client.name;
		client.geid = geid;

		world.queueTask([id, uuid, geid, name](World& world){ world.handlePlayerEnter(id, uuid, geid, name); });
	}

	void GameAcceptor::notifyLeave(ClientData& client)
	{
		auto& world = wmgr_->mainWorld();
		auto id = client.id;

		world.queueTask([id](World& world){ world.handlePlayerLeave(id); });
	}

	void GameAcceptor::startKeepAliveTimer()
	{
		keepAliveTimer_.expires_from_now(boost::posix_time::seconds(5));
		keepAliveTimer_.async_wait(
			[this](bs::error_code ec)
			{
				if(ec)
				{
					if(ec == ba::error::operation_aborted)
						return;

					throw bs::system_error(ec);
				}

				PacketKeepAlive keepAlive;
				keepAlive.id = std::rand() % 666;

				for(auto kv : clients_)
				{
					auto& client = *kv.value;
					auto buffer = buffers_.aquire();
					auto length = encodePacket(buffer.data(), PACKET_MAX_SIZE, client.compression, keepAlive);
					auto packet = SizedBuffer(std::move(buffer), length, {});

					if(!client.sendBuffers.empty())
					{
						auto& front = client.sendBuffers.front();
						auto oldFront = std::exchange(front, std::move(packet));
						client.sendBuffers.push(std::move(oldFront));
					}

					else
						client.sendBuffers.push(std::move(packet));

					startAsyncWrite(client);
					asyncWritePacket(*kv.value, keepAlive);
				}

				startKeepAliveTimer();
			});
	}

	void GameAcceptor::startAsyncWrite(ClientData& client)
	{
		if(!client.current.buffer.data() && !client.sendBuffers.empty())
		{
			client.current = std::move(client.sendBuffers.front());
			client.sendBuffers.pop();

			ba::async_write(client.socket, ba::buffer(client.current.buffer.data(), client.current.size), ba::transfer_all(),
				[this, &client](bs::error_code ec, SizeT)
				{
					if(ec)
						return;

					auto curr = std::move(client.current);

					startAsyncWrite(client);

					// must be the last operation, handler might delete client
					if(curr.handler)
						curr.handler(client);
				});
		}
	}

	void GameAcceptor::updateStatusResponsePacket()
	{
		statusResponse_.json = format(PING_JSON, VERSION, PROTOCOL_VERSION, slotsMax_, slotsUsed_, statusMessage_);
	}
}
