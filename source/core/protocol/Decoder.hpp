// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <algorithm>
#include <cstring>
#include <string>
#include <type_traits>

#include <boost/endian/conversion.hpp>

#include "GameTypes.hpp"
#include "protocol/VarInt.hpp"
#include "qsutils/FixedString.hpp"
#include "qsutils/StrongTypedefs.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	constexpr UInt32 STRING_MAX_LENGTH = 1024;

	namespace detail
	{
		template <typename T>
		T decodeVarIntImpl(UInt8 const* data, SizeT size)
		{
			data += size;
			T result = 0;

			while(size--)
				(result <<= 7) |= *--data;

			return result;
		}
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, UInt8* begin, UInt8* end)
	{
		QS_ASSERT(end >= begin, "[begin, end) is not a valid range")

		auto length = static_cast<SizeT>(end - begin);

		if(size < length)
			return false;

		std::memcpy(begin, buffer, length);
		buffer += length;
		size -= length;

		return true;
	}

	template <typename T, T V>
	bool decode(UInt8 const*& buffer, SizeT& size, Constant<T, V>)
	{
		T x;
		return decode(buffer, size, x) && x == V;
	}

	template <typename T>
	bool decode(UInt8 const*& buffer, SizeT& size, VarInt<T>& result)
	{
		static constexpr SizeT ITERATION_LIMIT = (sizeof result * 8 + 6) / 7;

		UInt8 bytes[ITERATION_LIMIT];

		for(SizeT i = 0; i != ITERATION_LIMIT; ++i)
		{
			if(i == size)
				return false;

			bytes[i] = *buffer & 0x7F;

			if(!(*buffer++ >> 7))
			{
				size -= i + 1;
				result = detail::decodeVarIntImpl<T>(bytes, i + 1);
				return true;
			}
		}

		return false;
	}

	template <typename T, typename DomainTag>
	bool decode(UInt8 const*& buffer, SizeT& size, VarInt<Integer<T, DomainTag>>& out)
	{
		VarInt<T> tmp;
		auto result = decode(buffer, size, tmp);

		if(result)
			out = varInt(Integer<T, DomainTag>(tmp.get()));

		return result;
	}

	template <typename Int, typename std::enable_if<std::is_integral<Int>::value>::type* = nullptr>
	bool decode(UInt8 const*& buffer, SizeT& size, Int& result)
	{
		if(size < sizeof result)
			return false;

		std::memcpy(&result, buffer, sizeof result);
		result = boost::endian::big_to_native(result);

		buffer += sizeof result;
		size -= sizeof result;

		return true;
	}

	template <typename Float, typename std::enable_if<std::is_floating_point<Float>::value>::type* = nullptr>
	bool decode(UInt8 const*& buffer, SizeT& size, Float& result)
	{
		if(size < sizeof result)
			return false;

		std::memcpy(&result, buffer, sizeof result);

		auto ptr = reinterpret_cast<char*>(&result);
		std::reverse(ptr, ptr + sizeof result);

		buffer += sizeof result;
		size -= sizeof result;

		return true;
	}

	template <typename T, typename DomainTag>
	bool decode(UInt8 const*& buffer, SizeT& size, Integer<T, DomainTag>& result)
	{
		T value;

		if(!decode(buffer, size, value))
			return false;

		result = Integer<T, DomainTag>(value);
		return true;
	}

	template <typename Enum, typename std::enable_if<std::is_enum<Enum>::value>::type* = nullptr>
	bool decode(UInt8 const*& buffer, SizeT& size, Enum& result)
	{
		using UT = typename std::underlying_type<Enum>::type;

		UT value;
		auto success = decode(buffer, size, value);

		if(!success)
			return false;

		result = static_cast<Enum>(value);
		return true;
	}

	template <typename Length, Length Capacity>
	bool decode(UInt8 const*& buffer, SizeT& size, FixedString<char, Length, Capacity>& result)
	{
		VarInt<UInt32> length;

		if(!decode(buffer, size, length))
			return false;

		if(length > Capacity || length > STRING_MAX_LENGTH || size < length)
			return false;

		FixedString<char, Length, Capacity> s(static_cast<Length>(length));
		std::memcpy(s.data(), buffer, length);
		result = s;

		buffer += length;
		size -= length;

		return true;
	}

	template <typename Alloc, typename Traits>
	bool decode(UInt8 const*& buffer, SizeT& size, std::basic_string<char, Alloc, Traits>& result)
	{
		VarInt<UInt32> length;

		if(!decode(buffer, size, length))
			return false;

		if(length > STRING_MAX_LENGTH || size < length)
			return false;

		std::string s;
		s.resize(length);
		std::memcpy(&s[0], buffer, length);

		result = std::move(s);

		buffer += length;
		size -= length;

		return true;
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, Uuid& uuid)
	{
		if(!decode(buffer, size, uuid.data, uuid.data + 16))
			return false;

		std::reverse(uuid.data, uuid.data + 16);
		return true;
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, bool& value)
	{
		UInt8 x;

		if(!decode(buffer, size, x) || x > 1)
			return false;

		value = x == 1 ? true : false;
		return true;
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, BlockCoord& coord)
	{
		UInt64 value;

		if(!decode(buffer, size, value))
			return false;

		coord.x() = value >> 38u;
		coord.y() = (value >> 26u) & 0xFFFu;
		coord.z() = value & 0x3FFFFFFu;

		if(coord.x() >= 1 << 25)
			coord.x() -= 1 << 26;

		if(coord.y() >= 1 << 11)
			coord.y() -= 1 << 12;

		if(coord.z() >= 1 << 25)
			coord.z() -= 1 << 26;

		return true;
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, EntityCoord& coord)
	{
		return decode(buffer, size, coord.x())
		    && decode(buffer, size, coord.y())
		    && decode(buffer, size, coord.z());
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, EntityLook& look)
	{
		return decode(buffer, size, look.yaw())
		    && decode(buffer, size, look.pitch());
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, ChunkCoord& coord)
	{
		return decode(buffer, size, coord.x())
		    && decode(buffer, size, coord.z());
	}

	bool decode(UInt8 const*& buffer, SizeT& size, Nbt& nbt);

	inline bool decode(UInt8 const*& buffer, SizeT& size, SlotData& slot)
	{
		if(!decode(buffer, size, slot.type))
			return false;

		if(slot.type == -1)
			return true;

		if(!decode(buffer, size, slot.count) || !decode(buffer, size, slot.damage))
			return false;

		if(size == 0)
			return false;

		if(*buffer == 0)
		{
			++buffer;
			--size;
			return true;
		}

		Nbt data;

		if(!decode(buffer, size, data))
			return false;

		slot.data = data;
		return true;
	}

	inline bool decode(UInt8 const*& buffer, SizeT& size, FixedEntityCoord& coord)
	{
		return decode(buffer, size, coord.x())
		    && decode(buffer, size, coord.y())
		    && decode(buffer, size, coord.z());
	}

	bool decode(UInt8 const*& buffer, SizeT& size, Metadata& data);
}
