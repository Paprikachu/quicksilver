// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <type_traits>
#include <utility>

#include <boost/numeric/conversion/cast.hpp>

#include "Chunk.hpp"
#include "GameTypes.hpp"
#include "protocol/Decoder.hpp"
#include "protocol/Encoder.hpp"
#include "protocol/WireLength.hpp"
#include "qsutils/Assertions.hpp"
#include "qsutils/BufferManager.hpp"
#include "qsutils/Compression.hpp"
#include "qsutils/Identity.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	#define QS_PACKET_BEGIN(name, sid, cid) struct name {
	#define QS_PACKET_MEMBER(name, ...) __VA_ARGS__ name;
	#define QS_PACKET_END() };

	#include "protocol/PacketSchemes.hpp"
	#include "protocol/PacketUndefMacros.hpp"

	#define QS_PACKET_BEGIN(name, sid, cid) \
		inline SizeT payloadWireLength(name const& packet) \
		{ \
			(void)packet; \
			SizeT size = 0;

	#define QS_PACKET_MEMBER(name, ...) size += wireLength(packet.name);
	#define QS_PACKET_END() return size; }

	#include "protocol/PacketSchemes.hpp"
	#include "protocol/PacketUndefMacros.hpp"

	#define QS_PACKET_BEGIN(name, sid, cid) \
		inline void encodePayload(UInt8*& buffer, name const& packet) \
		{ \
			(void)buffer; (void)packet;

	#define QS_PACKET_MEMBER(name, ...) encode(buffer, packet.name);
	#define QS_PACKET_END() }

	#include "protocol/PacketSchemes.hpp"
	#include "protocol/PacketUndefMacros.hpp"

	#define QS_PACKET_BEGIN(name, sid, cid) \
		inline bool decodePayload(UInt8 const*& buffer, SizeT& size, name& packet) \
		{ \
			(void)buffer; (void)size; (void)packet;

	#define QS_PACKET_MEMBER(name, ...) if(!decode(buffer, size, packet.name)) return false;
	#define QS_PACKET_END() return true; }

	#include "protocol/PacketSchemes.hpp"
	#include "protocol/PacketUndefMacros.hpp"

	#define QS_PACKET_BEGIN(name, sid, cid) constexpr Int32 serverPayloadIdOfImpl(qs::Identity<name>) { return sid; }

	#include "protocol/PacketSchemes.hpp"
	#include "protocol/PacketUndefMacros.hpp"

	#define QS_PACKET_BEGIN(name, sid, cid) constexpr Int32 clientPayloadIdOfImpl(qs::Identity<name>) { return cid; }

	#include "protocol/PacketSchemes.hpp"
	#include "protocol/PacketUndefMacros.hpp"

	struct PacketChunkData;

	constexpr Int32 serverPayloadIdOfImpl(Identity<PacketChunkData>) { return 0x21; }

	template <typename Payload>
	constexpr Int32 serverPayloadIdOf()
	{
		static_assert(serverPayloadIdOfImpl(Identity<Payload>()) != -1, "packet not a server packet");
		return serverPayloadIdOfImpl(Identity<Payload>());
	}

	constexpr Int32 clientPayloadIdOfImpl(Identity<PacketChunkData>) { return -1; }

	template <typename Payload>
	constexpr Int32 clientPayloadIdOf()
	{
		static_assert(clientPayloadIdOfImpl(Identity<Payload>()) != -1, "packet not a client packet");
		return clientPayloadIdOfImpl(Identity<Payload>());
	}

	struct PacketChunkData
	{
		ChunkCoord coord;
		bool continuous;
		UInt16 sectionMask;
		VarInt<Int32> dataLength;
		bool skyLightNotSent;
		Chunk* chunk;
	};

	inline SizeT payloadWireLength(PacketChunkData const& packet)
	{
		SizeT length = 0;
		length += wireLength(packet.coord);
		length += wireLength(packet.continuous);
		length += wireLength(packet.sectionMask);
		length += wireLength(packet.dataLength);

		if(packet.chunk)
			length += wireLength(*packet.chunk, packet.skyLightNotSent);

		return length;
	}

	inline void encodePayload(UInt8*& buffer, PacketChunkData const& packet)
	{
		encode(buffer, packet.coord);
		encode(buffer, packet.continuous);
		encode(buffer, packet.sectionMask);
		encode(buffer, packet.dataLength);

		if(packet.chunk)
 		{
			for(SizeT i = 0; i != 16; ++i)
			{
				if(packet.chunk->sectionBitmap() & (1 << i))
				{
					auto blocks = reinterpret_cast<UInt8 const*>(packet.chunk->sectionBlockData(i));
					encode(buffer, blocks, blocks + CHUNK_SECTION_BLOCKS_SIZE);
				}
			}

			for(SizeT i = 0; i != 16; ++i)
			{
				if(packet.chunk->sectionBitmap() & (1 << i))
				{
					auto blockLight = packet.chunk->sectionBlockLightData(i);
					encode(buffer, blockLight, blockLight + CHUNK_SECTION_BLOCKLIGHT_SIZE);
				}
			}

			if(packet.skyLightNotSent)
			{
				for(SizeT i = 0; i != 16; ++i)
				{
					if(packet.chunk->sectionBitmap() & (1 << i))
					{
						auto skyLight = packet.chunk->sectionSkyLightData(i);
						encode(buffer, skyLight, skyLight + CHUNK_SECTION_SKYLIGHT_SIZE);
					}
				}
			}

			auto biomes = packet.chunk->biomeData();
			encode(buffer, biomes, biomes + CHUNK_BIOME_SIZE);
		}
	}

	constexpr Int32 const PACKET_MAX_SIZE = 1024;

	struct PacketHeader
	{
		VarInt<Int32> length;
		VarInt<Int32> type;
		UInt8 const* payload;
	};

	struct Packet
	{
		PacketHeader header;
		BufferManager::UniqueRef buffer;
	};

	enum struct PacketDecodeResult
	{
		NEED_MORE_BYTES,
		INVALID_LENGTH,
		SUCCESS,
	};

	inline PacketDecodeResult tryDecodePacket(UInt8 const*& buffer, SizeT& size, bool compression, PacketHeader& packet)
	{
		VarInt<Int32> length;

		if(!decode(buffer, size, length))
			return PacketDecodeResult::NEED_MORE_BYTES;

		auto sizeAfterLength = size;

		if(compression)
		{
			VarInt<Int32> uncompressedSize;

			if(!decode(buffer, size, uncompressedSize))
				return PacketDecodeResult::NEED_MORE_BYTES;

			if(uncompressedSize != 0)
				return PacketDecodeResult::INVALID_LENGTH;
		}

		if(!decode(buffer, size, packet.type))
			return PacketDecodeResult::NEED_MORE_BYTES;

		packet.length = length - boost::numeric_cast<Int32>(sizeAfterLength - size);
		packet.payload = buffer;

		if(packet.length > PACKET_MAX_SIZE || packet.length < 0)
			return PacketDecodeResult::INVALID_LENGTH;

		if(static_cast<UInt32>(packet.length) > size)
			return PacketDecodeResult::NEED_MORE_BYTES;

		buffer += packet.length;
		size -= packet.length;

		return PacketDecodeResult::SUCCESS;
	}

	// please for the love of god, don't ever touch this code again, it *will* break in mysterious ways
	template <typename Payload>
	SizeT encodePacket(UInt8* buffer, SizeT size, bool compression, Payload const& payload)
	{
		if(compression)
		{
			auto typeLength = wireLength(varInt(serverPayloadIdOf<Payload>()));
			auto payloadLength = payloadWireLength(payload);
			auto dataLength = typeLength + payloadLength;

			if(dataLength > PACKET_MAX_SIZE)
			{
				auto uncompressedLengthLength = wireLength(varInt(dataLength));
				auto lengthLength = wireLength(varInt(uncompressedLengthLength + dataLength));
				auto tmptotalLength = lengthLength + uncompressedLengthLength + dataLength;

				if(tmptotalLength > size)
					throw std::runtime_error("buffer not large enough");

				auto buf = buffer;
				encode(buf, varInt(serverPayloadIdOf<Payload>()));
				encodePayload(buf, payload);

				auto compressed = zlibCompress(buffer, dataLength);

				auto length = uncompressedLengthLength + compressed.size();
				encode(buffer, varInt(length));
				encode(buffer, varInt(dataLength));
				encode(buffer, compressed.data(), compressed.data() + compressed.size());

				return wireLength(varInt(length)) + length;
			}

			else
			{
				auto uncompressedLengthLength = wireLength(varInt(0));
				auto lengthLength = wireLength(varInt(uncompressedLengthLength + dataLength));
				auto totalLength = lengthLength + uncompressedLengthLength + dataLength;

				if(totalLength > size)
					throw std::runtime_error("buffer not large enough");

				encode(buffer, varInt(uncompressedLengthLength + dataLength));
				encode(buffer, varInt(0));
				encode(buffer, varInt(serverPayloadIdOf<Payload>()));
				encodePayload(buffer, payload);

				return totalLength;
			}
		}

		else
		{
			auto payloadLength = payloadWireLength(payload);
			auto typeLength = wireLength(varInt(serverPayloadIdOf<Payload>()));
			auto length = typeLength + payloadLength;
			auto totalLength = wireLength(varInt(length)) + length;

			if(totalLength > size)
				throw std::runtime_error("buffer not large enough");

			encode(buffer, varInt(length));
			encode(buffer, varInt(serverPayloadIdOf<Payload>()));
			encodePayload(buffer, payload);

			return totalLength;
		}
	}

	template <typename Payload>
	bool decodePayload(Packet const& packet, Payload& payload)
	{
		SizeT size = packet.header.length;
		UInt8 const* buffer = packet.header.payload;

		return decodePayload(buffer, size, payload) && size == 0;
	}
}
