// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <string>
#include <type_traits>

#include <boost/uuid/uuid.hpp>

#include "Chunk.hpp"
#include "GameTypes.hpp"
#include "protocol/VarInt.hpp"
#include "qsutils/CountLeadingZeros.hpp"
#include "qsutils/FixedString.hpp"
#include "qsutils/PopCount.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	template <typename Value, typename std::enable_if<std::is_arithmetic<Value>::value>::type* = nullptr>
	SizeT wireLength(Value value)
	{
		return sizeof value;
	}

	template <typename T, typename DomainTag>
	SizeT wireLength(Integer<T, DomainTag> value)
	{
		return sizeof value.value();
	}

	namespace detail
	{
		template <typename T>
		SizeT highestBit(T value)
		{
			return sizeof value * 8 - 1 - countLeadingZeros(value);
		}

		constexpr UInt8 hbTable[] =
		{
			1, 1, 1, 1, 1, 1, 1,
			2, 2, 2, 2, 2, 2, 2,
			3, 3, 3, 3, 3, 3, 3,
			4, 4, 4, 4, 4, 4, 4,
			5, 5, 5, 5, 5, 5, 5,
			6, 6, 6, 6, 6, 6, 6,
			7, 7, 7, 7, 7, 7, 7,
			8, 8, 8, 8, 8, 8, 8,
			9, 9, 9, 9, 9, 9, 9,
			10,
		};

		static_assert(sizeof hbTable == 64, "table incorrect");
	}

	template <typename T>
	SizeT wireLength(VarInt<T> value)
	{
		static_assert(sizeof value <= 8, "highest bit table only works up until 64 bit");

		if(value == 0)
			return 1;

		return detail::hbTable[detail::highestBit(value.get())];
	}

	template <typename T, T V>
	SizeT wireLength(Constant<T, V>)
	{
		return wireLength(V);
	}

	template <typename T, typename DomainTag>
	SizeT wireLength(VarInt<Integer<T, DomainTag>> value)
	{
		return wireLength(varInt(value.get().value()));
	}

	template <typename Char, typename Length, Length Capacity>
	SizeT wireLength(FixedString<Char, Length, Capacity> const& s)
	{
		return s.length() + wireLength(VarInt<Length>(s.length()));
	}

	template <typename Char, typename Alloc, typename Traits>
	SizeT wireLength(std::basic_string<Char, Alloc, Traits> const& s)
	{
		using SizeType = typename std::basic_string<Char, Alloc, Traits>::size_type;
		return s.length() + wireLength(VarInt<SizeType>(s.length()));
	}

	inline SizeT wireLength(Uuid uuid)
	{
		return sizeof uuid;
	}

	template <typename Enum, typename std::enable_if<std::is_enum<Enum>::value>::type* = nullptr>
	SizeT wireLength(Enum value)
	{
		using UT = typename std::underlying_type<Enum>::type;
		return wireLength(static_cast<UT>(value));
	}

	inline SizeT wireLength(bool b)
	{
		return sizeof b;
	}

	inline SizeT wireLength(BlockCoord)
	{
		return sizeof(UInt64);
	}

	inline SizeT wireLength(EntityCoord coord)
	{
		return wireLength(coord.x()) + wireLength(coord.y()) + wireLength(coord.z());
	}

	inline SizeT wireLength(EntityLook look)
	{
		return wireLength(look.yaw()) + wireLength(look.pitch());
	}

	inline SizeT wireLength(ChunkCoord coord)
	{
		return wireLength(coord.x()) + wireLength(coord.z());
	}

	inline SizeT wireLength(Chunk const& chunk, bool skyLight)
	{
		auto sections = popcount(chunk.sectionBitmap());

		SizeT length = 0;
		length += wireLength(chunk.coord());
		length += wireLength(chunk.sectionBitmap());

		length += sections * CHUNK_SECTION_SIZE_NO_SKYLIGHT;

		if(skyLight)
			length += sections * CHUNK_SECTION_SKYLIGHT_SIZE;

		length += CHUNK_BIOME_SIZE;

		return length;
	}

	SizeT wireLength(Nbt const& nbt);

	inline SizeT wireLength(SlotData const& slot)
	{
		if(slot.type == -1)
			return wireLength(slot.type);

		auto length = wireLength(slot.type) + wireLength(slot.count) + wireLength(slot.damage);
		length += slot.data ? wireLength(slot.data.get()) : 1;
		return length;
	}

	inline SizeT wireLength(FixedEntityCoord const& coord)
	{
		return wireLength(coord.x()) + wireLength(coord.y()) + wireLength(coord.z());
	}

	SizeT wireLength(Metadata const& data);
}
