// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "GameTypes.hpp"
#include "qsutils/Assertions.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct Inventory
	{
		static constexpr SizeT const SLOT_COUNT = 45;
		static constexpr SizeT const FIRST_SELECTABLE_SLOT = 36;
		
		SlotData& operator [] (UInt16 index)
		{
			return const_cast<SlotData&>(static_cast<Inventory const&>(*this)[index]);
		}

		SlotData const& operator [] (UInt16 index) const
		{
			QS_ASSERT(index < SLOT_COUNT, "index out of range")
			return slots_[index];
		}

		UInt16 selectedIndex() const { return selected_; }
		void selectedIndex(UInt16 index) { selected_ = index; }

		SlotData& selectedSlot() { return slots_[selected_]; }
		SlotData const& selectedSlot() const { return slots_[selected_]; }

	private:
		SlotData slots_[SLOT_COUNT];
		UInt16 selected_ = FIRST_SELECTABLE_SLOT;
	};
}