// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <queue>
#include <tuple>
#include <utility>

#include "GameTypes.hpp"
#include "protocol/Packets.hpp"
#include "qsutils/Buffer.hpp"
#include "qsutils/BufferManager.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	struct Player;
	struct World;

	struct WorldNetHandler
	{
		static constexpr SizeT BUFFER_SIZE = 1024;
		static constexpr SizeT HUGE_BUFFER_SIZE = 1024 * 200;

		WorldNetHandler(World& world);

		template <typename Payload>
		void queuePacket(ClientId cid, Payload const& payload)
		{
			auto buffer = bmgr_.aquire();
			auto size = encodePacket(buffer.data(), BUFFER_SIZE, true, payload);
			outgoingPackets_.push(std::make_tuple(cid, std::move(buffer), size));
		}

		template <typename Payload>
		void queueHugePacket(ClientId cid, Payload const& payload)
		{
			auto buffer = hbmgr_.aquire();
			auto size = encodePacket(buffer.data(), HUGE_BUFFER_SIZE, true, payload);
			outgoingPackets_.push(std::make_tuple(cid, std::move(buffer), size));
		}

		void preTick();
		void postTick();

	private:
		void handlePacket(EntityPtr<Player> player, Packet& packet);

		World* world_;
		BufferManager bmgr_;
		BufferManager hbmgr_;
		std::queue<std::tuple<ClientId, BufferManager::UniqueRef, SizeT>> outgoingPackets_;
	};
}
