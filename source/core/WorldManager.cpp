// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <boost/filesystem.hpp>

#include "WorldManager.hpp"
#include "qsutils/JsonUtils.hpp"
#include "qsutils/Logging.hpp"

namespace qs
{
	WorldManager::WorldManager(MinecraftServer& server, Json::Value const& config)
		: server_(&server)
	{
		auto worldFolder = bfs::path("worlds/");

		if(!bfs::exists(worldFolder))
			bfs::create_directory(worldFolder);

		if(!bfs::is_directory(worldFolder))
			throw std::runtime_error(format("%s is not a directory", worldFolder));

		bfs::directory_iterator begin(worldFolder), end;

		for(; begin != end; ++begin)
			if(bfs::is_directory(*begin))
				loadWorld(begin->path().filename().string());

		auto it = worldsByName_.find(find<std::string>(config, "main-world"));

		if(it == worldsByName_.end())
			throw std::runtime_error("main world not found");

		mainWorld_ = it->second;
	}

	WorldId WorldManager::loadWorld(std::string const& name)
	{
		auto id = idGenerator_.createId();
		auto world = std::make_unique<World>(*server_, id, name);
		worldsByName_.insert(std::make_pair(name, &*world));
		worlds_.emplace(id, std::move(world));
		return id;
	}

	World& WorldManager::mainWorld()
	{
		return *mainWorld_;
	}
}
