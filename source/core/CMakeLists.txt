include_directories(.)

file(GLOB_RECURSE HEADERS ./*.hpp)
file(GLOB_RECURSE SOURCES ./*.cpp)

set(QS_CORE_LIBS
	${Boost_DATE_TIME_LIBRARY}
	${Boost_FILESYSTEM_LIBRARY}
	${Boost_SYSTEM_LIBRARY}
	jsoncpp
	qsutils
)

if(UNIX)
	set(QS_CORE_LIBS ${QS_CORE_LIBS} dl pthread)
endif()

set(QS_CORE_LIBS ${QS_CORE_LIBS} PARENT_SCOPE)

add_library(core OBJECT ${HEADERS} ${SOURCES})
qs_set_cxx_compiler_settings(core)