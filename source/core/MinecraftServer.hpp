// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <functional>
#include <set>
#include <string>

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/container/flat_set.hpp>

#include "GameTypes.hpp"
#include "Rcon.hpp"
#include "WorldManager.hpp"
#include "protocol/GameAcceptor.hpp"
#include "qsutils/IdGenerator.hpp"
#include "qsutils/NamespaceShorthands.hpp"
#include "qsutils/Types.hpp"

namespace qs
{
	using namespace std::placeholders;

	struct MinecraftServer
	{
		MinecraftServer(ba::io_service& service, Json::Value const& config)
			: service_(&service)
			, worldManager_(*this, config)
			, rcon_(service, config, std::bind(&MinecraftServer::rconCommandHandler, this, _1, _2))
			, gameAcceptor_(service, config, worldManager_, *this)
		{}

		template <typename F>
		void queueTask(F&& f)
		{
			service_->post(std::forward<F>(f));
		}

		template <typename F>
		void popPackets(bc::flat_set<ClientId> ids, F&& f)
		{
			auto sp = std::make_shared<bc::flat_set<ClientId>>(std::move(ids));
			queueTask([this, sp, f]{ gameAcceptor_.popPackets(*sp, f); });
		}

		void pushPackets(std::queue<std::tuple<ClientId, BufferManager::UniqueRef, SizeT>> packets)
		{
			auto sp = std::make_shared<std::queue<std::tuple<ClientId, BufferManager::UniqueRef, SizeT>>>(std::move(packets));
			queueTask([this, sp]{ gameAcceptor_.pushPackets(std::move(*sp)); });
		}

		void disconnect(ClientId id)
		{
			queueTask([this, id]{ gameAcceptor_.disconnect(id); });
		}

		void disconnectWithMessage(ClientId id, std::string const& message)
		{
			queueTask([this, id, message]{ gameAcceptor_.disconnectWithMessage(id, message); });
		}

		GlobalEntityId createGlobalEntityId()
		{
			std::lock_guard<std::mutex> _(idGenMutex_);
			return entityIdGenerator_.createId();
		}

		void destroyGlobalEntityId(GlobalEntityId id)
		{
			std::lock_guard<std::mutex> _(idGenMutex_);
			entityIdGenerator_.destroyId(id);
		}

	private:
		void rconCommandHandler(std::string command, std::function<void(std::string const&)> respond)
		{
			if(command == "stop")
				service_->stop();

			else if(command == "ping")
				respond("pong");
		}

		ba::io_service* service_;

		mutable std::mutex idGenMutex_;
		IdGenerator<GlobalEntityId> entityIdGenerator_;

		WorldManager worldManager_;

		Rcon rcon_;
		GameAcceptor gameAcceptor_;
	};
}
