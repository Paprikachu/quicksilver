// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#ifdef _WIN32
#define QSAPI_EXPORT extern "C" __declspec(dllexport)
#else
#define QSAPI_EXPORT extern "C" __attribute__((visibility("default")))
#endif

#define QSAPI_GENERATOR(type) \
	QSAPI_EXPORT \
	void* create(qsapi::UInt64 seed, char const* parameters, qsapi::GeneratorData data) \
	{ \
		return new type(seed, parameters, data); \
	}\
	\
	QSAPI_EXPORT \
	void destroy(void* generator) \
	{ \
		delete static_cast<qsapi::Generator*>(generator); \
	} \
	\
	QSAPI_EXPORT \
	void generate(void* generator, qsapi::ChunkCoord coord, qsapi::ChunkData data) \
	{ \
		static_cast<qsapi::Generator*>(generator)->generate(coord, data); \
	} \
	\
	QSAPI_EXPORT \
	void populate(void* generator, qsapi::ChunkCoord coord, qsapi::ChunkData data) \
	{ \
		static_cast<qsapi::Generator*>(generator)->populate(coord, data); \
	}

namespace qsapi
{
	typedef signed char Int8;
	typedef short Int16;
	typedef int Int32;
	typedef long long Int64;

	typedef unsigned char UInt8;
	typedef unsigned short UInt16;
	typedef unsigned UInt32;
	typedef unsigned long long UInt64;

	struct ChunkCoord
	{
		Int32 x, z;
	};

	struct ChunkData
	{
		UInt16 (* blocks) [256][16][16];
		UInt8 (* biomes) [16][16];
	};

	struct GeneratorData
	{
		void* context;
		ChunkData (* findOrGenerateChunk) (void* context, ChunkCoord coord);
	};

	struct Generator
	{
		virtual void generate(qsapi::ChunkCoord coord, qsapi::ChunkData data) = 0;
		virtual void populate(qsapi::ChunkCoord coord, qsapi::ChunkData data) { (void)coord; (void)data; }
		virtual ~Generator() {}

	protected:
		Generator(GeneratorData data)
			: data_(data)
		{}

		ChunkData findOrGenerateChunk(ChunkCoord coord)
		{
			return data_.findOrGenerateChunk(data_.context, coord);
		}

	private:
		GeneratorData data_;
	};

#define BLOCKID(type, meta) ((type << 4) | meta)
	enum BlockType
	{
		AIR               = BLOCKID(0, 0),
		STONE             = BLOCKID(1, 0),
		GRANITE           = BLOCKID(1, 1),
		GRANITE_POLISHED  = BLOCKID(1, 2),
		DIORITE           = BLOCKID(1, 3),
		DIORITE_POLISHED  = BLOCKID(1, 4),
		ANDESITE          = BLOCKID(1, 5),
		ANDESITE_POLISHED = BLOCKID(1, 6),
		GRASS             = BLOCKID(2, 0),
		DIRT              = BLOCKID(3, 0),
		DIRT_COARSE       = BLOCKID(3, 1),
		DIRT_PODZOL       = BLOCKID(3, 2),
		COBBLESTONE       = BLOCKID(4, 0),
		PLANK_OAK         = BLOCKID(5, 0),
		PLANK_SPRUCE      = BLOCKID(5, 1),
		PLANK_BIRCH       = BLOCKID(5, 2),
		PLANK_JUNGLE      = BLOCKID(5, 3),
		PLANK_ACACIA      = BLOCKID(5, 4),
		PLANK_DARK_OAK    = BLOCKID(5, 5),
		SAPLING_OAK       = BLOCKID(6, 0),
		SAPLING_SPRUCE    = BLOCKID(6, 1),
		SAPLING_BIRCH     = BLOCKID(6, 2),
		SAPLING_JUNGLE    = BLOCKID(6, 3),
		SAPLING_ACACIA    = BLOCKID(6, 4),
		SAPLING_DARK_OAK  = BLOCKID(6, 5),
		BEDROCK           = BLOCKID(7, 0),
		WATER_FLOWING     = BLOCKID(8, 0),
		WATER_STILL       = BLOCKID(9, 0),
		LAVA_FLOWING      = BLOCKID(10, 0),
		LAVA_STILL        = BLOCKID(11, 0),
		SAND_WHITE        = BLOCKID(12, 0),
		SAND_RED          = BLOCKID(12, 1),
		GRAVEL            = BLOCKID(13, 0),
		ORE_GOLD          = BLOCKID(14, 0),
		ORE_IRON          = BLOCKID(15, 0),
		ORE_COAL          = BLOCKID(16, 0),
		WOOD_OAK          = BLOCKID(17, 0),
		WOOD_SPRUCE       = BLOCKID(17, 1),
		WOOD_BIRCH        = BLOCKID(17, 2),
		WOOD_JUNGLE       = BLOCKID(17, 3),
		LEAVES_OAK        = BLOCKID(18, 0),
		LEAVES_SPRUCE     = BLOCKID(18, 1),
		LEAVES_BIRCH      = BLOCKID(18, 2),
		LEAVES_JUNGLE     = BLOCKID(18, 3),
		SPONGE_DRY        = BLOCKID(19, 0),
		SPONGE_WET        = BLOCKID(19, 1),
	};
#undef BLOCKID

	enum Biome
	{
		NETHER                =   8,
		END                   =   9,

		OCEAN                 =   0,
		OCEAN_FROZEN          =  10,
		OCEAN_DEEP            =  24,

		PLAINS                =   1,
		PLAINS_SUNFLOWER      = 129,

		DESERT                =   2,
		DESERT_HILLS          =  17,
		DESERT_M              = 130,

		EXTREME_HILLS         =   3,
		EXTREME_HILLS_EDGE    =  20,
		EXTREME_HILLS_PLUS    =  34,
		EXTREME_HILLS_M       = 131,
		EXTREME_HILLS_PLUS_M  = 162,

		FOREST                =   4,
		FOREST_HILLS          =  18,
		FOREST_BIRCH          =  27,
		FOREST_BIRCH_HILLS    =  28,
		FOREST_ROOFED         =  29,
		FOREST_FLOWER         = 132,
		FOREST_BIRCH_M        = 155,
		FOREST_BIRCH_HILLS_M  = 156,
		FOREST_ROOFED_M       = 157,

		TAIGA                 =   5,
		TAIGA_HILLS           =  19,
		TAIGA_COLD            =  30,
		TAIGA_COLD_HILLS      =  31,
		TAIGA_MEGA            =  32,
		TAIGA_MEGA_HILLS      =  33,
		TAIGA_M               = 133,
		TAIGA_COLD_M          = 158,
		TAIGA_MEGA_SPRUCE     = 160,
		TAIGA_REDWOOD_HILLS_M = 161,

		SWAMPLAND             =   6,
		SWAMPLAND_M           = 134,

		RIVER                 =   7,
		RIVER_FROZEN          =  11,

		ICE_PLAINS            =  12,
		ICE_MOUNTAINS         =  13,
		ICE_PLAINS_SPIKES     = 140,

		MUSHROOM_ISLAND       =  14,
		MUSHROOM_ISLAND_SHORE =  15,

		BEACH                 =  16,
		BEACH_STONE           =  25,
		BEACH_COLD            =  26,

		JUNGLE                =  21,
		JUNGLE_HILLS          =  22,
		JUNGLE_EDGE           =  23,
		JUNGLE_M              = 149,
		JUNGLE_EDGE_M         = 151,

		SAVANNA               =  35,
		SAVANNA_PLATEAU       =  36,
		SAVANNA_M             = 163,
		SAVANNA_PLATEAU_M     = 164,

		MESA                  =  37,
		MESA_PLATEAU_F        =  38,
		MESA_PLATEAU          =  39,
		MESA_BRYCE            = 165,
		MESA_PLATEAU_F_M      = 166,
		MESA_PLATEAU_M        = 167,
	};
}
