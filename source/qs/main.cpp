// Copyright 2015 Markus Grech
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <csignal>
#include <exception>

#include <boost/asio/io_service.hpp>
#include <boost/asio/signal_set.hpp>

#include <json/json.h>

#include "MinecraftServer.hpp"
#include "qsutils/JsonUtils.hpp"
#include "qsutils/Logging.hpp"

int main()
try
{
	using namespace qs;

	// for writes to asio socket
	// when the other end closes connection, SIGPIPE is raised (as opposed to just getting an error_code)
	// this prevents the signal and instead gets us the desired behaviour
#ifdef SIGPIPE
	std::signal(SIGPIPE, SIG_IGN);
#endif

	ba::io_service service;

	ba::signal_set signals(service);
	signals.add(SIGTERM);

#ifdef SIGQUIT
	signals.add(SIGQUIT);
#endif

#ifdef SIGHUP
	signals.add(SIGHUP);
#endif

	signals.add(SIGINT);

	signals.async_wait(
		[&](bs::error_code, int)
		{
			info("signal received - shutting down server ...");
			service.stop();
		});

	auto config = loadJsonFromFile("config.json");
	info("config loaded");
	MinecraftServer server(service, config);

	service.run();
}

catch(std::exception const& e)
{
	qs::error("%s", e.what());
	return -1;
}
