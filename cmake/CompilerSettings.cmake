include(CheckCXXCompilerFlag)

function(qs_append_cmode_flags TARGET)
	if(QS_GCCLIKE)
		target_compile_options(${TARGET} PRIVATE -std=c11)
	endif()
endfunction()

function(qs_append_cxxmode_flags TARGET)
	if(QS_GCCLIKE)
		check_cxx_compiler_flag(-std=c++1z HAS_STD_CXX1Z)
		if(HAS_STD_CXX1Z)
			target_compile_options(${TARGET} PRIVATE -std=c++1z)
			return()
		endif()

		check_cxx_compiler_flag(-std=c++14 HAS_STD_CXX14)
		if(HAS_STD_CXX14)
			target_compile_options(${TARGET} PRIVATE -std=c++14)
			return()
		endif()

		check_cxx_compiler_flag(-std=c++1y HAS_STD_CXX1Y)
		if(HAS_STD_CXX1Y)
			target_compile_options(${TARGET} PRIVATE -std=c++1y)
			return()
		endif()

		check_cxx_compiler_flag(-std=c++11 HAS_STD_CXX11)
		if(HAS_STD_CXX11)
			target_compile_options(${TARGET} PRIVATE -std=c++11)
			return()
		endif()

		check_cxx_compiler_flag(-std=c++0x HAS_STD_CXX0X)
		if(HAS_STD_CXX0X)
			target_compile_options(${TARGET} PRIVATE -std=c++0x)
			return()
		endif()
	endif()
endfunction()

function(qs_append_warning_flags TARGET)
	if(MSVC)
		target_compile_options(${TARGET} PRIVATE /W4)
		target_compile_options(${TARGET} PRIVATE /wd4351) # "new behaviour: elements of array will be default initialized"
		target_compile_options(${TARGET} PRIVATE /wd4800) # "forcing value to bool 'true' or 'false' (performance warning)"
		target_compile_options(${TARGET} PRIVATE /wd4100) # "unreferenced formal parameter"
		target_compile_options(${TARGET} PRIVATE /wd4512) # "assignment operator was implicitly defined as deleted"
		target_compile_options(${TARGET} PRIVATE /wd4457) # "declaration hides function parameter"
		target_compile_options(${TARGET} PRIVATE /wd4005) # "macro redefinition" (bug in boost.asio)
		target_compile_options(${TARGET} PRIVATE /wd4127) # "conditional expression is constant"
		target_compile_options(${TARGET} PRIVATE /wd4244) # conversion warnings

	elseif(QS_GCCLIKE)
		target_compile_options(${TARGET} PRIVATE -pedantic -Werror -Wall -Wextra)
		target_compile_options(${TARGET} PRIVATE -Wcast-align -Wcast-qual -Wformat=2)
		target_compile_options(${TARGET} PRIVATE -Wstrict-overflow=4)
		target_compile_options(${TARGET} PRIVATE -Wunreachable-code -Wno-sign-conversion -Wno-missing-field-initializers)
		target_compile_options(${TARGET} PRIVATE -Wpointer-arith -Winit-self)
	endif()
endfunction()

function(qs_append_defines TARGET)
	if(MSVC)
		target_compile_definitions(${TARGET} PRIVATE
			NOMINMAX
			UNICODE
			_UNICODE
			_CRT_SECURE_NO_WARNINGS
			_SCL_SECURE_NO_WARNINGS
			BOOST_ALL_NO_LIB
		)
	endif()

	if(WIN32)
		target_compile_definitions(${TARGET} PRIVATE
			_WIN32_WINNT=0x0601
		)
	endif()
endfunction()

function(qs_append_misc_flags TARGET)
	if(MSVC)
		target_compile_options(${TARGET} PRIVATE /MP)
	elseif(QS_GCCLIKE)
		target_compile_options(${TARGET} PRIVATE
			-march=native
			-fstack-protector
			-Wl,-z,now
			-Wl,--no-undefined
			-Wl,--build-id=sha1
		)
	endif()
endfunction()

function(qs_set_c_compiler_settings TARGET)
	qs_append_misc_flags(${TARGET})
	qs_append_cmode_flags(${TARGET})
	qs_append_warning_flags(${TARGET})
	qs_append_defines(${TARGET})
endfunction()

function(qs_set_cxx_compiler_settings TARGET)
	qs_append_misc_flags(${TARGET})
	qs_append_cxxmode_flags(${TARGET})
	qs_append_warning_flags(${TARGET})
	qs_append_defines(${TARGET})
endfunction()