add_subdirectory(zlib-ng)
add_subdirectory(jsoncpp)

set(gtest_force_shared_crt ON)
add_subdirectory(googletest)

set_target_properties(gtest_main PROPERTIES
	EXCLUDE_FROM_ALL 1
	EXCLUDE_FROM_DEFAULT_BUILD 1
)