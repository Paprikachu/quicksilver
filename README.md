# Minecraft server, written in C++ #
An attempt to create a fully functional server for the popular game Minecraft, but with better performance than vanilla servers and its derivatives like Bukkit and SpigotMC.

A plugin API is planned, but currently only custom terrain generators are supported. An API header can be found under source/api. The terrain generator API has no dependencies (not even the standard library as of now) and even supports C++98/03 if you feel like inflicting pain to yourself.